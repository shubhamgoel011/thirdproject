package pop_replay;

import java.util.Iterator;
import java.util.List;

import soot.Local;
import soot.LongType;
import soot.jimple.InterfaceInvokeExpr;
import soot.PatchingChain;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootMethod;
import soot.jimple.StaticFieldRef;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.VirtualInvokeExpr;
import soot.util.Chain;
import soot.Unit;
import soot.Type;
import soot.jimple.AssignStmt;

import java.util.Map;

import pop_replay.ConstantUtils;
import soot.Body;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;

public class PoP_Replay_Instrumentor extends SceneTransformer {
	static SootMethod  beforeThreadStart, criticalBefore,
			criticalAfter;
	static {
		Scene.v().addBasicClass("pop_replay.PoP_Replay_Util");
	}

	public static final String BEGIN = "Begin";
	public static final String READ = "Read";
	public static final String LOCK = "Lock";
	public static final String FORK = "Fork";
	public static final String JOIN = "Join";
	public static final String UNLOCK = "Unlock";
	public static final String WRITE = "Write";
	public static final String END = "End";

	private static void instrumentCriticalEvent(String criticalEventType,
			Body body, Stmt statement, boolean type) {
		// TODO Auto-generated method stub
		if (type) {
			body.getUnits().insertBefore(
					Jimple.v().newInvokeStmt(
							Jimple.v().newStaticInvokeExpr(
									criticalBefore.makeRef(),
									StringConstant.v(criticalEventType))),
					statement);
		}

		else {
			body.getUnits().insertAfter(
					Jimple.v().newInvokeStmt(
							Jimple.v().newStaticInvokeExpr(
									criticalAfter.makeRef(),
									StringConstant.v(criticalEventType))),
					statement);

		}

	}

	@Override
	protected void internalTransform(String arg0, Map<String, String> arg1) {
		Chain<SootClass> allClasses = Scene.v().getApplicationClasses();

		for (SootClass curClass : allClasses) {

			if (curClass.getName().contains("pop_replay.PoP_Replay_Util")) {
				beforeThreadStart = curClass
						.getMethod("void beforeThreadStart(long)");
				criticalBefore = curClass
						.getMethod("void criticalBefore(java.lang.String)");
				criticalAfter = curClass
						.getMethod("void criticalAfter(java.lang.String)");
			}
		}

		for (SootClass curClass : allClasses) {

			/* These classes must be skipped */
			if (curClass.getName().contains("pop_replay.PoP_Replay_Util")
					|| curClass.getName().contains("popUtil.PoP_Util")
					|| !curClass.getName().contains("test")) {
				continue;
			}
			List<SootMethod> allMethods = curClass.getMethods();
			for (SootMethod curMethod : allMethods) {
				Body body = curMethod.retrieveActiveBody();

				Local childThreadId = Jimple.v().newLocal(
						ConstantUtils.CHILD_THREAD_ID, LongType.v());
				body.getLocals().add(childThreadId);
				PatchingChain<Unit> units = body.getUnits();
				Iterator<Unit> statementIterator = units.snapshotIterator();
				
				while (statementIterator.hasNext()) {

					Stmt statement = (Stmt) statementIterator.next();

					if (statement instanceof InvokeStmt) {
						InvokeExpr invokeExpr = statement.getInvokeExpr();
						if (invokeExpr instanceof VirtualInvokeExpr) {
							if (((VirtualInvokeExpr) invokeExpr).getMethod()
									.getSignature()
									.equals(ConstantUtils.THREAD_FORK)) {

								Local temporary = (Local) ((VirtualInvokeExpr) invokeExpr)
										.getBase();
								body.getUnits()
										.insertBefore(
												Jimple.v()
														.newAssignStmt(
																childThreadId,
																Jimple.v()
																		.newVirtualInvokeExpr(
																				temporary,
																				Scene.v()
																						.getMethod(
																								ConstantUtils.THREAD_GETID_METHOD_NAME)
																						.makeRef())),
												statement);
								body.getUnits().insertBefore(
										Jimple.v().newInvokeStmt(
												Jimple.v().newStaticInvokeExpr(
														beforeThreadStart
																.makeRef(),
														childThreadId)),
										statement);

								instrumentCriticalEvent(FORK, body, statement,
										true);
								instrumentCriticalEvent(FORK, body, statement,
										false);
							} else if (((VirtualInvokeExpr) invokeExpr)
									.getMethod().getSignature()
									.equals(ConstantUtils.THREAD_JOIN)) {
								instrumentCriticalEvent(JOIN, body, statement,
										true);
								instrumentCriticalEvent(JOIN, body, statement,
										false);
							}
						} else if (invokeExpr instanceof InterfaceInvokeExpr) {
							if (((InterfaceInvokeExpr) invokeExpr).getMethod()
									.getSignature()
									.equals(ConstantUtils.THREAD_LOCK)) {
								instrumentCriticalEvent(LOCK, body, statement,
										true);
								instrumentCriticalEvent(LOCK, body, statement,
										false);
							} else if (((InterfaceInvokeExpr) invokeExpr)
									.getMethod().getSignature()
									.equals(ConstantUtils.THREAD_UNLOCK)) {
								instrumentCriticalEvent(UNLOCK, body,
										statement, true);
								instrumentCriticalEvent(UNLOCK, body,
										statement, false);
							}
						}
					} else if (statement instanceof AssignStmt) {
						AssignStmt assignStatement = (AssignStmt) statement;
						boolean checkType;
						Type type = assignStatement.getLeftOp().getType();
						checkType = false;

						for (String string : ConstantUtils.validTypes) {
							if (type.toString().equals(string)) {
								checkType = true;
								break;
							}
						}

						if (checkType
								&& assignStatement.getRightOp() instanceof StaticFieldRef) {
							instrumentCriticalEvent(READ, body, statement, true);
							instrumentCriticalEvent(READ, body, statement,
									false);

						} else if (assignStatement.getLeftOp() instanceof StaticFieldRef
								&& !assignStatement.getLeftOp().getType()
										.toString().contains("lock")) {
							instrumentCriticalEvent(WRITE, body, statement,
									true);
							instrumentCriticalEvent(WRITE, body, statement,
									false);
						}
					}
				}

			}
		}
	}
}
