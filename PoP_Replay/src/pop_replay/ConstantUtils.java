package pop_replay;

public class ConstantUtils {

	public static final String BALL_LARUS_PATH_IDENTIFIER = "ballLarusPathIdentifier";
	public static final String DIRECTORY_NAME = "Output";
	public static final String DAG_GRAPH_SERIALIZABLE_FILE = "DAGGraph.ser";

	public static final String METHOD_INVOCATION_NUMBER_BY_THIS_THREAD = "methodInvocationNumberByThisThread";
	public static final String THREAD_START_METHOD_NAME = "<java.lang.Thread: void start()>";
	public static final String THREAD_GETID_METHOD_NAME = "<java.lang.Thread: long getId()>";
	
	public static final String POP_UTIL_CLASS_NAME = "PoP_Util";
	public static final String NUMBER_OF_TIMES_THIS_METHOD_CALLED_BY_THIS_THREAD = "numOfTimesThisMethodCalledByThisThread";
	public static final String CHILD_THREAD_ID = "childThreadId";
	public static final String THREAD_ID = "threadId";

	
	public static final Object THREAD_FORK = "<java.lang.Thread: void start()>";
	public static final Object THREAD_JOIN = "<java.lang.Thread: void join()>";
	
	public static final Object THREAD_LOCK = "<java.util.concurrent.locks.Lock: void lock()>";
	public static final Object THREAD_UNLOCK = "<java.util.concurrent.locks.Lock: void unlock()>";
	
	public static final String METHOD_INVOKED = "<test";
	
	public static final String[] validTypes = { "java.lang.Byte",
			"java.lang.Boolean", "java.lang.Character", "java.lang.Integer",
			"java.lang.Long", "java.lang.Double", "byte", "boolean", "char",
			"int", "long", "double" };
	
	public static final String SYM_LOCAL_WRITE = "Local";
	public static final String SYM_SHARED_WRITE = "SymValWrite";
	public static final String SYM_SHARED_READ = "SymValRead";
	public static final String order = "O";

	private ConstantUtils() {
		super();
		// TODO Auto-generated constructor stub
	}

}