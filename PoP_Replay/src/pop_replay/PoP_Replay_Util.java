package pop_replay;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class PoP_Replay_Util {

	public static final String BEGIN = "Begin";
	public static final String END = "End";
	public static boolean mutex = true;
	public static Integer monitor = new Integer(0);
	public static String[] threadIdMapping = new String[10000];
	public static int[] maxChildNum = new int[10000];
	public static int index=0;
	public static ArrayList<String> globalThreadIdSequence = new ArrayList<String>();
	
	static {

		String in = "";
		for (int i = 0; i < threadIdMapping.length; i++) {
			threadIdMapping[i] = "0";
		}
		
		try {
			in = new String(Files.readAllBytes(Paths.get("global_trace")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] splitString = in.split(System.getProperty("line.separator"));
		for (String tupleString : splitString) {
			String[] tupleSplitString = tupleString.split(",");
			if (!(tupleSplitString[1].trim().equals(BEGIN) || tupleSplitString[1]
					.trim().equals(END))) {

				globalThreadIdSequence.add(tupleSplitString[0].trim());
			}
		}
		
		
		
	}

	public static void beforeThreadStart(long currentThreadID) {
		int parentThreadId = (int) Thread.currentThread().getId();
		threadIdMapping[(int) currentThreadID] = threadIdMapping[parentThreadId]

				+ "." + maxChildNum[parentThreadId];
		maxChildNum[parentThreadId] += 1;
	}

	/* You can modify criticalBefore() to receive the required arguments */
	public static void criticalBefore(String criticalEventType)
			throws InterruptedException {
		synchronized (monitor) {
			int currentThreadId = (int) Thread.currentThread().getId();

			String artificialThreadId = threadIdMapping[currentThreadId];
			if (artificialThreadId == null)
				artificialThreadId = "0";

			while (true) {
				if (mutex == true
						&& globalThreadIdSequence.get(index).equals(
								artificialThreadId)) {
					mutex = false;
					index++;
			break;
				}
				monitor.wait();

			}

		}
	}

	/* You can modify criticalAfter() to receive the required arguments */
	public static void criticalAfter(String criticalEventType) {
		synchronized (monitor) {
			mutex = true;
			monitor.notifyAll();
		}

	}

}
