package e0210.soot;

import java.util.HashMap;
import java.util.Hashtable;

public class Helper {

	private static final int maxNumofThreads = 10000;

	private static boolean isMainInitialized = false;

	/* Hashtable<methodHashCode, methodName> */
	public static Hashtable<Integer, String> methodNameMappingToHashCode = new Hashtable<Integer, String>();
	/*
	 * String[genratedthreadId] = DefinedThreadId We want in linear fashion only
	 * which thread is called next
	 */

	public static String[] generatedThreadIDMappingToDefinedThreadIDStructure = new String[maxNumofThreads];
	/* int[genratedthreadId] = numOfThreadsSpannedByThisThread */
	public static int[] numOfThreadsSpannedByThisThread = new int[maxNumofThreads];
	/*
	 * numOfTimesThisMethodCalledByThisThread =
	 * (List.get(threadId)).get(methodHashCode)
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<Integer, Integer>[] numOfTimesThisMethodCalledByThisThread = new HashMap[maxNumofThreads];
	/* int[genratedthreadId] = numOfMethodsInvokedByThisThread */
	public static int[] numOfMethodsInvokedByThisThread = new int[maxNumofThreads];

	public static synchronized void initializeMainMethodInvocation(
			String methodName) {
		if (!isMainInitialized) {
			long currentThreadId = Thread.currentThread().getId();
			generatedThreadIDMappingToDefinedThreadIDStructure[(int) currentThreadId] = "0";
			isMainInitialized = true;
		}
	}

	public static synchronized void threadStartMethodInvoked(
			long currentThreadID) {
		long parentThreadId = Thread.currentThread().getId();
		generatedThreadIDMappingToDefinedThreadIDStructure[(int) currentThreadID] = generatedThreadIDMappingToDefinedThreadIDStructure[(int) parentThreadId].toString()
				+ "." + String.valueOf(numOfThreadsSpannedByThisThread[(int) parentThreadId]);
		numOfThreadsSpannedByThisThread[(int) parentThreadId] += 1;
	}

	public static synchronized void methodInvokedInThread(String methodName) {
		long currentThreadId = Thread.currentThread().getId();
		methodNameMappingToHashCode.put(methodName.hashCode(), methodName);

		if (numOfTimesThisMethodCalledByThisThread[(int) currentThreadId] == null){
			HashMap<Integer, Integer> hashMap = new HashMap<>();
			numOfTimesThisMethodCalledByThisThread[(int) currentThreadId] = hashMap;
		}
		int numOfPreviousInvocations;
		if (numOfTimesThisMethodCalledByThisThread[(int) currentThreadId]
				.containsKey(methodName.hashCode()))
			numOfPreviousInvocations = numOfTimesThisMethodCalledByThisThread[(int) currentThreadId]
					.get(methodName.hashCode());
		else
			numOfPreviousInvocations = 0;
		numOfTimesThisMethodCalledByThisThread[(int) currentThreadId].put(
				methodName.hashCode(), numOfPreviousInvocations + 1);
		numOfMethodsInvokedByThisThread[(int) currentThreadId] += 1;
	}

	public static synchronized long getNumOfTimesThisMethodCalledByThisThread(
			String methodName) {
		long currentThreadId = Thread.currentThread().getId();
		
		if (numOfTimesThisMethodCalledByThisThread[(int) currentThreadId] == null)
			return 0;
		else if (!numOfTimesThisMethodCalledByThisThread[(int) currentThreadId]
				.containsKey(methodName.hashCode()))
			return 0;
		return numOfTimesThisMethodCalledByThisThread[(int) currentThreadId].get(methodName.hashCode());

	}

	public static synchronized long getNumOfMethodsInvokedByThisThread(
			String methodName) {
		long currentThreadId = Thread.currentThread().getId();
		return numOfMethodsInvokedByThisThread[(int) currentThreadId];
	}

	public static synchronized void printThreadLocalMethodTuple(
			String methodName, long numOfTimesThisMethodCalledByThisThread,
			long numOfMethodsInvokedByThisThread,
			long ballLarusIdentificationNumber) {
		long currentThreadId = Thread.currentThread().getId();
		ThreadLocalMethodTupleBean threadLocalMethodTupleBean = new ThreadLocalMethodTupleBean(
				methodName.hashCode(),
				methodName,
				(generatedThreadIDMappingToDefinedThreadIDStructure[(int) currentThreadId] == null? "0" : generatedThreadIDMappingToDefinedThreadIDStructure[(int) currentThreadId]),
				(int) numOfTimesThisMethodCalledByThisThread,
				(int) numOfMethodsInvokedByThisThread,
				(int) ballLarusIdentificationNumber);
		System.out.println(threadLocalMethodTupleBean);
	}

	public static synchronized void printMethodBallLarusIdentifier(
			long methodBallLarusIdentifier) {
		System.out.println(methodBallLarusIdentifier);
	}

}
