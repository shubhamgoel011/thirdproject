package e0210.soot;

import java.util.ArrayList;
import java.util.List;

public class ThreadLocalMethodTupleBean {

	public int methodHashCode;
	public String methodName;
	public String threadId;
	public int methodInvocationNumberAmongThisMethodCalls;
	public int methodInvocationNumberAmongThisThreadCalls;
	public int ballLarusIdentificationNumber;
	public List<Integer> basicBlockSequenceNumber = new ArrayList<Integer>();

	public ThreadLocalMethodTupleBean(int methodHashCode, String methodName,
			String threadId, int methodInvocationNumberAmongThisMethodCalls,
			int methodInvocationNumberAmongThisThreadCalls,
			int ballLarusIdentificationNumber,
			List<Integer> basicBlockSequenceNumber) {
		super();
		this.methodHashCode = methodHashCode;
		this.methodName = methodName;
		this.threadId = threadId;
		this.methodInvocationNumberAmongThisMethodCalls = methodInvocationNumberAmongThisMethodCalls;
		this.methodInvocationNumberAmongThisThreadCalls = methodInvocationNumberAmongThisThreadCalls;
		this.ballLarusIdentificationNumber = ballLarusIdentificationNumber;
		this.basicBlockSequenceNumber = basicBlockSequenceNumber;
	}

	public ThreadLocalMethodTupleBean(int methodHashCode, String methodName,
			String threadId, int methodInvocationNumberAmongThisMethodCalls,
			int methodInvocationNumberAmongThisThreadCalls,
			int ballLarusIdentificationNumber) {
		super();

		this.methodHashCode = methodHashCode;
		this.methodName = methodName;
		this.threadId = threadId;
		this.methodInvocationNumberAmongThisMethodCalls = methodInvocationNumberAmongThisMethodCalls;
		this.methodInvocationNumberAmongThisThreadCalls = methodInvocationNumberAmongThisThreadCalls;
		this.ballLarusIdentificationNumber = ballLarusIdentificationNumber;
	}

	public ThreadLocalMethodTupleBean() {
		super();
	}

	@Override
	public String toString() {
		return methodName + "," + threadId + ","
				+ methodInvocationNumberAmongThisMethodCalls + ","
				+ ballLarusIdentificationNumber + ","
				+ methodInvocationNumberAmongThisThreadCalls;
	}

	public String toTupleString() {
		// TODO Auto-generated method stub
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Thread ID:").append(threadId).append("  Method Name:").append(methodName).append(" Invocation Number:").append(methodInvocationNumberAmongThisThreadCalls).append(" Basic Block ID:");
		for(Integer basicBlockSequenceString: basicBlockSequenceNumber)
			stringBuilder.append("->").append(basicBlockSequenceString);
		stringBuilder.append("\n");
		return stringBuilder.toString();
	}

}
