package e0210;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.jgrapht.graph.DirectedWeightedPseudograph;

import e0210.Utilities.Utilities;
import e0210.Utilities.bean.DAGGraph;
import e0210.Utilities.bean.VertexBlock;
import e0210.graph.CustomWeightedEdge;

public class ProcessOutput {

	public static void main(String[] args) throws IOException {

		String project = args[0];
		String testcase = args[1];

		String inPath = "Testcases/" + project + "/output/" + testcase;
		String outPath = "Testcases/" + project + "/processed-output/"
				+ testcase;

		System.out.println("Processing " + testcase + " of " + project);

		// Read the contents of the output file into a string
		String in = new String(Files.readAllBytes(Paths.get(inPath)));
		StringBuilder stringBuilder = new StringBuilder();

		/*
		 * 
		 * Write your algorithm which does the post-processing of the output
		 */

		String[] splitString = in.split(System.getProperty("line.separator"));
		int[] ballLarusIdentifiers = new int[splitString.length];
		int iterator = 0;

		for (String string : splitString)
			ballLarusIdentifiers[iterator++] = Integer.parseInt(string);

		List<DAGGraph> dagGraphList = Utilities.loadDAGFromFile();

		DAGGraph previousDagGraph = new DAGGraph(-1, -1, null);
		boolean firstLineAdded = false;
		for (iterator = 0; iterator < ballLarusIdentifiers.length; iterator++) {
			DAGGraph dagGraph = getDAGGraphForBLIdentifier(dagGraphList,
					ballLarusIdentifiers[iterator]);
			ArrayList<Integer> blockIds = computeBlockIDsForCoreespondingBLIdentifiers(
					dagGraph.getSerializableDirectedWeightedPseudograph(),
					(int) (ballLarusIdentifiers[iterator] - (int) dagGraph
							.getStartBallLarusIdentifier()));

			int firstBlockIndexInMethod = getFirstBlockIDForCoreespondingBLIdentifiers(
					dagGraph.getSerializableDirectedWeightedPseudograph(),
					(int) (ballLarusIdentifiers[iterator] - (int) dagGraph
							.getStartBallLarusIdentifier()));

			if (firstLineAdded
					&& previousDagGraph.getStartBallLarusIdentifier() != -1
					&& !(previousDagGraph.getStartBallLarusIdentifier() == dagGraph
							.getStartBallLarusIdentifier() && previousDagGraph
							.getEndBallLarusIdentifier() == dagGraph
							.getEndBallLarusIdentifier()))
				stringBuilder.append(System.getProperty("line.separator"));
			else if (firstLineAdded && (firstBlockIndexInMethod == 0))
				stringBuilder.append(System.getProperty("line.separator"));

			for (Integer blockId : blockIds) {
				if (firstLineAdded == false)
					firstLineAdded = true;
				else
					stringBuilder.append(System.getProperty("line.separator"));
				stringBuilder.append(blockId);
			}

			previousDagGraph = dagGraph;
		}

		// Write the contents of the string to the output file
		PrintWriter out = new PrintWriter(outPath);
		out.print(stringBuilder.toString());

		out.close();

		return;
	}

	private static int getFirstBlockIDForCoreespondingBLIdentifiers(
			DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> directedWeightedPseudograph,
			int ballLarusIdentifier) {
		// TODO Auto-generated method stub
		VertexBlock dummyStartNode = null, tempNode;
		int edgeWeight, previousEdgeWeight;

		for (VertexBlock vertexBlock : directedWeightedPseudograph.vertexSet()) {
			if (vertexBlock.getVertexBlockIndex() == -1)
				dummyStartNode = vertexBlock;
		}

		tempNode = dummyStartNode;
		CustomWeightedEdge tempCustomWeightedEdge = null;

		previousEdgeWeight = -1;
		for (CustomWeightedEdge customWeightedEdge : directedWeightedPseudograph
				.outgoingEdgesOf(tempNode)) {
			edgeWeight = (int) directedWeightedPseudograph
					.getEdgeWeight(customWeightedEdge);
			if (edgeWeight <= ballLarusIdentifier
					&& edgeWeight > previousEdgeWeight) {
				previousEdgeWeight = edgeWeight;
				tempCustomWeightedEdge = customWeightedEdge;
			}
		}
		tempNode = directedWeightedPseudograph
				.getEdgeTarget(tempCustomWeightedEdge);
		return tempNode.getVertexBlockIndex();
	}

	private static ArrayList<Integer> computeBlockIDsForCoreespondingBLIdentifiers(
			DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> directedWeightedPseudograph,
			int ballLarusIdentifier) {
		// TODO Auto-generated method stub
		VertexBlock dummyEndNode = null, dummyStartNode = null, tempNode;
		ArrayList<Integer> blockIds = new ArrayList<Integer>();
		int edgeWeight, previousEdgeWeight;

		for (VertexBlock vertexBlock : directedWeightedPseudograph.vertexSet()) {
			if (vertexBlock.getVertexBlockIndex() == -1)
				dummyStartNode = vertexBlock;
			else if (vertexBlock.getVertexBlockIndex() == (directedWeightedPseudograph
					.vertexSet().size() - 2))
				dummyEndNode = vertexBlock;
		}

		tempNode = dummyStartNode;
		CustomWeightedEdge tempCustomWeightedEdge = null;

		while (tempNode.getVertexBlockIndex() != dummyEndNode
				.getVertexBlockIndex()) {
			if (!(tempNode.getVertexBlockIndex() == dummyStartNode
					.getVertexBlockIndex() || tempNode.getVertexBlockIndex() == dummyEndNode
					.getVertexBlockIndex()))
				blockIds.add(tempNode.getVertexBlockIndex());
			if (tempNode.isHasExitOrReturnStatement())
				return blockIds;
			previousEdgeWeight = -1;
			for (CustomWeightedEdge customWeightedEdge : directedWeightedPseudograph
					.outgoingEdgesOf(tempNode)) {
				edgeWeight = (int) directedWeightedPseudograph
						.getEdgeWeight(customWeightedEdge);
				if (edgeWeight <= ballLarusIdentifier
						&& edgeWeight > previousEdgeWeight) {
					previousEdgeWeight = edgeWeight;
					tempCustomWeightedEdge = customWeightedEdge;
				}
			}
			tempNode = directedWeightedPseudograph
					.getEdgeTarget(tempCustomWeightedEdge);
			ballLarusIdentifier -= ((int) directedWeightedPseudograph
					.getEdgeWeight(tempCustomWeightedEdge));
		}

		return blockIds;
	}

	private static DAGGraph getDAGGraphForBLIdentifier(
			List<DAGGraph> dagGraphList, int ballLarusIdentifier) {
		// TODO Auto-generated method stub

		for (DAGGraph dagGraph : dagGraphList) {
			if (ballLarusIdentifier >= dagGraph.getStartBallLarusIdentifier()
					&& ballLarusIdentifier < dagGraph
							.getEndBallLarusIdentifier())
				return dagGraph;
		}
		return null;
	}
}