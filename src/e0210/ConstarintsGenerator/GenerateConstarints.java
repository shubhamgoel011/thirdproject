package e0210.ConstarintsGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;

import e0210.Utilities.ConstantUtils;
import e0210.Utilities.bean.CriticalEventTypes;
import e0210.Utilities.bean.ThreadLocalTupleBean;

public class GenerateConstarints {

	private static Context context;

	public static void setContext(Context contextParameter) {
		context = contextParameter;
	}

	public static List<BoolExpr> generateProgramOrderConstarints(
			HashMap<String, List<ThreadLocalTupleBean>> intraThreadTrace) {
		// TODO Auto-generated method stub
		List<BoolExpr> programOrderConstarints = new ArrayList<BoolExpr>();

		for (String threadId : intraThreadTrace.keySet()) {
			for (int i = 1; i < intraThreadTrace.get(threadId).size(); i++) {
				ThreadLocalTupleBean threadLocalTupleBeanPrevious = intraThreadTrace
						.get(threadId).get(i - 1);
				ThreadLocalTupleBean threadLocalTupleBeanCurrent = intraThreadTrace
						.get(threadId).get(i);
				ArithExpr arithExpr1 = context.mkIntConst(ConstantUtils.order
						+ "_" + threadLocalTupleBeanPrevious.threadId + "_"
						+ String.valueOf(i - 1));
				ArithExpr arithExpr2 = context.mkIntConst(ConstantUtils.order
						+ "_" + threadLocalTupleBeanCurrent.threadId + "_"
						+ String.valueOf(i));
				BoolExpr boolExpr = context.mkLt(arithExpr1, arithExpr2);
				programOrderConstarints.add(boolExpr);
			}
		}

		return programOrderConstarints;
	}

	public static List<BoolExpr> generateForkJoinConstarints(
			HashMap<String, List<ThreadLocalTupleBean>> intraThreadTrace) {
		// TODO Auto-generated method stub
		List<BoolExpr> forkJoinConstarints = new ArrayList<BoolExpr>();

		for (String threadId : intraThreadTrace.keySet()) {
			for (int i = 0; i < intraThreadTrace.get(threadId).size(); i++) {
				ThreadLocalTupleBean threadLocalTupleBean = intraThreadTrace
						.get(threadId).get(i);
				ArithExpr arithExpr1, arithExpr2;
				if (threadLocalTupleBean.criticalEventType
						.equals(CriticalEventTypes.FORK)) {
					arithExpr1 = context.mkIntConst(ConstantUtils.order + "_"
							+ threadLocalTupleBean.threadId + "_"
							+ String.valueOf(i));
					arithExpr2 = context.mkIntConst(ConstantUtils.order + "_"
							+ threadLocalTupleBean.childThreadID + "_"
							+ String.valueOf(0));
					BoolExpr boolExpr = context.mkLt(arithExpr1, arithExpr2);
					forkJoinConstarints.add(boolExpr);
				} else if (threadLocalTupleBean.criticalEventType
						.equals(CriticalEventTypes.JOIN)) {
					arithExpr1 = context
							.mkIntConst(ConstantUtils.order
									+ "_"
									+ threadLocalTupleBean.childThreadID
									+ "_"
									+ String.valueOf(intraThreadTrace.get(
											threadLocalTupleBean.childThreadID)
											.size() - 1));
					arithExpr2 = context.mkIntConst(ConstantUtils.order + "_"
							+ threadLocalTupleBean.threadId + "_"
							+ String.valueOf(i));
					BoolExpr boolExpr = context.mkLt(arithExpr1, arithExpr2);
					forkJoinConstarints.add(boolExpr);
				}
			}
		}
		return forkJoinConstarints;
	}

	public static List<BoolExpr> generateLockUnlockConstraints(
			HashMap<String, List<ThreadLocalTupleBean>> intraThreadTrace) {
		// TODO Auto-generated method stub
		List<BoolExpr> lockUnlockConstarints = new ArrayList<BoolExpr>();
		String threadIds[] = new String[intraThreadTrace.keySet().size()];
		threadIds = intraThreadTrace.keySet().toArray(threadIds);

		for (int currentThreadIndex = 0; currentThreadIndex < threadIds.length; currentThreadIndex++) {
			String currentThreadId = threadIds[currentThreadIndex];

			for (int currentLockIndex = 0; currentLockIndex < intraThreadTrace
					.get(currentThreadId).size(); currentLockIndex++) {
				ThreadLocalTupleBean currentLockThreadLocalTupleBean = intraThreadTrace
						.get(currentThreadId).get(currentLockIndex);
				if (currentLockThreadLocalTupleBean.criticalEventType
						.equals(CriticalEventTypes.LOCK)) {
					ArithExpr arithExprLock1 = null, arithExprUnlock1 = null, arithExprLock2 = null, arithExprUnlock2 = null;
					String lockObjectName = currentLockThreadLocalTupleBean.lockObjectName;

					arithExprLock1 = context.mkIntConst(ConstantUtils.order
							+ "_" + currentLockThreadLocalTupleBean.threadId
							+ "_" + String.valueOf(currentLockIndex));
					for (int currentUnlockIndex = currentLockIndex + 1; currentUnlockIndex < intraThreadTrace
							.get(currentThreadId).size(); currentUnlockIndex++) {
						ThreadLocalTupleBean currentUnlockThreadLocalTupleBean = intraThreadTrace
								.get(currentThreadId).get(currentUnlockIndex);
						if (currentUnlockThreadLocalTupleBean.criticalEventType
								.equals(CriticalEventTypes.UNLOCK)
								&& currentUnlockThreadLocalTupleBean.lockObjectName
										.equals(lockObjectName)) {
							arithExprUnlock1 = context
									.mkIntConst(ConstantUtils.order
											+ "_"
											+ currentUnlockThreadLocalTupleBean.threadId
											+ "_"
											+ String.valueOf(currentUnlockIndex));
							break;
						}
					}
					// Lock Unlock Object pair Retreived

					// Now matching with other threads Lock Unlock Pair

					for (int nextThreadIndex = currentThreadIndex + 1; nextThreadIndex < threadIds.length; nextThreadIndex++) {
						String nextThreadId = threadIds[nextThreadIndex];
						for (int nextLockIndex = 0; nextLockIndex < intraThreadTrace
								.get(nextThreadId).size(); nextLockIndex++) {
							ThreadLocalTupleBean nextLockThreadLocalTupleBean = intraThreadTrace
									.get(nextThreadId).get(nextLockIndex);
							if (nextLockThreadLocalTupleBean.criticalEventType
									.equals(CriticalEventTypes.LOCK)
									&& nextLockThreadLocalTupleBean.lockObjectName
											.equals(lockObjectName)) {
								arithExprLock2 = context
										.mkIntConst(ConstantUtils.order
												+ "_"
												+ nextLockThreadLocalTupleBean.threadId
												+ "_"
												+ String.valueOf(nextLockIndex));
								for (int nextUnlockIndex = nextLockIndex + 1; nextUnlockIndex < intraThreadTrace
										.get(nextThreadId).size(); nextUnlockIndex++) {
									ThreadLocalTupleBean nextUnlockThreadLocalTupleBean = intraThreadTrace
											.get(nextThreadId).get(
													nextUnlockIndex);
									if (nextUnlockThreadLocalTupleBean.criticalEventType
											.equals(CriticalEventTypes.UNLOCK)
											&& nextUnlockThreadLocalTupleBean.lockObjectName
													.equals(lockObjectName)) {
										arithExprUnlock2 = context
												.mkIntConst(ConstantUtils.order
														+ "_"
														+ nextUnlockThreadLocalTupleBean.threadId
														+ "_"
														+ String.valueOf(nextUnlockIndex));
										BoolExpr boolExpr1 = context.mkLt(
												arithExprUnlock1,
												arithExprLock2);
										BoolExpr boolExpr2 = context.mkLt(
												arithExprUnlock2,
												arithExprLock1);
										BoolExpr boolExpr = context.mkOr(
												boolExpr1, boolExpr2);
										lockUnlockConstarints.add(boolExpr);
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		return lockUnlockConstarints;
	}

	public static List<BoolExpr> generateReadWriteConstraints(
			HashMap<String, List<ThreadLocalTupleBean>> intraThreadTrace) {
		// TODO Auto-generated method stub
		List<BoolExpr> readWriteConstraints = new ArrayList<BoolExpr>();

		for (String threadId : intraThreadTrace.keySet()) {
			for (int readIndex = 0; readIndex < intraThreadTrace.get(threadId)
					.size(); readIndex++) {
				ThreadLocalTupleBean readThreadLocalTupleBean = intraThreadTrace
						.get(threadId).get(readIndex);
				if (readThreadLocalTupleBean.criticalEventType
						.equals(CriticalEventTypes.READ)) {
					ArithExpr arithExprRead = context
							.mkIntConst(ConstantUtils.order + "_"
									+ readThreadLocalTupleBean.threadId + "_"
									+ String.valueOf(readIndex));
					List<BoolExpr> readConstraints = new ArrayList<BoolExpr>();
					BoolExpr boolExpr = null;
					// Finding Write Shared in each thread
					for (String writeTreadId : intraThreadTrace.keySet()) {
						for (int writeIndex = 0; writeIndex < intraThreadTrace
								.get(writeTreadId).size(); writeIndex++) {
							ThreadLocalTupleBean writeThreadLocalTupleBean = intraThreadTrace
									.get(writeTreadId).get(writeIndex);
							if (writeThreadLocalTupleBean.criticalEventType
									.equals(CriticalEventTypes.WRITE)
									&& readThreadLocalTupleBean.sharedVariableName
											.equals(writeThreadLocalTupleBean.sharedVariableName)) {
								ArithExpr arithExprWrite = context
										.mkIntConst(ConstantUtils.order
												+ "_"
												+ writeThreadLocalTupleBean.threadId
												+ "_"
												+ String.valueOf(writeIndex));
								boolExpr = context
										.mkAnd(context
												.mkEq(context
														.mkIntConst(readThreadLocalTupleBean.sharedVariableSymbolicName),
														context.mkIntConst(writeThreadLocalTupleBean.sharedVariableSymbolicName)),
												context.mkLt(arithExprWrite,
														arithExprRead));

								// Finding other write to same shared variable
								// as read other than this write in any thread
								for (String otherWriteThreadId : intraThreadTrace
										.keySet()) {
									for (int otherWriteIndex = 0; otherWriteIndex < intraThreadTrace
											.get(otherWriteThreadId).size(); otherWriteIndex++) {
										ThreadLocalTupleBean otherWriteThreadLocalTupleBean = intraThreadTrace
												.get(otherWriteThreadId).get(
														otherWriteIndex);
										if (otherWriteThreadLocalTupleBean.criticalEventType
												.equals(CriticalEventTypes.WRITE)
												&& readThreadLocalTupleBean.sharedVariableName
														.equals(otherWriteThreadLocalTupleBean.sharedVariableName)
												&& !(writeThreadLocalTupleBean.threadId
														.equals(otherWriteThreadLocalTupleBean.threadId) && writeIndex == otherWriteIndex)) {
											ArithExpr arithExprOtherWrite = context
													.mkIntConst(ConstantUtils.order
															+ "_"
															+ otherWriteThreadLocalTupleBean.threadId
															+ "_"
															+ String.valueOf(otherWriteIndex));
											BoolExpr otherwriteBoolExpr = context
													.mkOr(context
															.mkLt(arithExprOtherWrite,
																	arithExprWrite),
															context.mkGt(
																	arithExprOtherWrite,
																	arithExprRead));
											boolExpr = context.mkAnd(boolExpr,
													otherwriteBoolExpr);
										}
									}
								}
								readConstraints.add(boolExpr);
							}
						}
					}
					BoolExpr tempBoolExpr = null;
					for (BoolExpr readConstraint : readConstraints) {
						if (tempBoolExpr == null)
							tempBoolExpr = readConstraint;
						else
							tempBoolExpr = context.mkOr(tempBoolExpr, readConstraint);
					}
					readWriteConstraints.add(tempBoolExpr);
				}
			}
		}

		return readWriteConstraints;
	}
}
