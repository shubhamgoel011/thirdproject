package e0210.BasicBlockSequenceTrace;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.graph.DirectedWeightedPseudograph;

import e0210.Utilities.Utilities;
import e0210.Utilities.bean.DAGGraph;
import e0210.Utilities.bean.VertexBlock;
import e0210.graph.CustomWeightedEdge;
import e0210.soot.ThreadLocalMethodTupleBean;

public class ProcessOutputToGenerateThreadTuple {

	public static ThreadTraceWalk generateThreadTuple(String in) {
		ThreadTraceWalk threadTraceWalk = new ThreadTraceWalk();
		/*
		 * 
		 * Write your algorithm which does the post-processing of the output
		 */
		List<DAGGraph> dagGraphList = Utilities.loadDAGFromFile();
		String[] splitString = in.split(System.getProperty("line.separator"));
		for (String tupleString : splitString) {
			ArrayList<Integer> blockIds;
			boolean tupleAdded = false;

			String[] tupleSplitString = tupleString.split(",");
			ThreadLocalMethodTupleBean tempThreadLocalMethodTupleBean = new ThreadLocalMethodTupleBean(
					tupleSplitString[0].hashCode(), tupleSplitString[0],
					tupleSplitString[1], Integer.parseInt(tupleSplitString[2]),
					Integer.parseInt(tupleSplitString[4]),
					Integer.parseInt(tupleSplitString[3]));
			for (ThreadLocalMethodTupleBean threadLocalMethodTupleBean : threadTraceWalk.threadLocalMethodTupleBeans) {
				if (threadLocalMethodTupleBean.methodHashCode == tempThreadLocalMethodTupleBean.methodHashCode
						&& threadLocalMethodTupleBean.methodName
								.equals(tempThreadLocalMethodTupleBean.methodName)
						&& threadLocalMethodTupleBean.threadId
								.equals(tempThreadLocalMethodTupleBean.threadId)
						&& threadLocalMethodTupleBean.methodInvocationNumberAmongThisThreadCalls == tempThreadLocalMethodTupleBean.methodInvocationNumberAmongThisThreadCalls) {
					DAGGraph dagGraph = getDAGGraphForBLIdentifier(
							dagGraphList,
							tempThreadLocalMethodTupleBean.ballLarusIdentificationNumber);
					blockIds = computeBlockIDsForCoreespondingBLIdentifiers(
							dagGraph.getSerializableDirectedWeightedPseudograph(),
							(int) (tempThreadLocalMethodTupleBean.ballLarusIdentificationNumber - (int) dagGraph
									.getStartBallLarusIdentifier()));
					for (Integer blockId : blockIds)
						threadLocalMethodTupleBean.basicBlockSequenceNumber
								.add(blockId);
					tupleAdded = true;
					break;
				}
			}
			if (!tupleAdded) {
				ThreadLocalMethodTupleBean threadLocalMethodTupleBean = threadTraceWalk
						.addThreadLocalMethodTuple(tupleSplitString[0],
								tupleSplitString[1], tupleSplitString[2],
								tupleSplitString[3], tupleSplitString[4]);
				DAGGraph dagGraph = getDAGGraphForBLIdentifier(
						dagGraphList,
						tempThreadLocalMethodTupleBean.ballLarusIdentificationNumber);
				blockIds = computeBlockIDsForCoreespondingBLIdentifiers(
						dagGraph.getSerializableDirectedWeightedPseudograph(),
						(int) (tempThreadLocalMethodTupleBean.ballLarusIdentificationNumber - (int) dagGraph
								.getStartBallLarusIdentifier()));
				threadLocalMethodTupleBean.basicBlockSequenceNumber = blockIds;
			}
		}
		return threadTraceWalk;
	}

	private static ArrayList<Integer> computeBlockIDsForCoreespondingBLIdentifiers(
			DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> directedWeightedPseudograph,
			int ballLarusIdentifier) {
		// TODO Auto-generated method stub
		VertexBlock dummyEndNode = null, dummyStartNode = null, tempNode;
		ArrayList<Integer> blockIds = new ArrayList<Integer>();
		int edgeWeight, previousEdgeWeight;

		for (VertexBlock vertexBlock : directedWeightedPseudograph.vertexSet()) {
			if (vertexBlock.getVertexBlockIndex() == -1)
				dummyStartNode = vertexBlock;
			else if (vertexBlock.getVertexBlockIndex() == (directedWeightedPseudograph
					.vertexSet().size() - 2))
				dummyEndNode = vertexBlock;
		}

		tempNode = dummyStartNode;
		CustomWeightedEdge tempCustomWeightedEdge = null;

		while (tempNode.getVertexBlockIndex() != dummyEndNode
				.getVertexBlockIndex()) {
			if (!(tempNode.getVertexBlockIndex() == dummyStartNode
					.getVertexBlockIndex() || tempNode.getVertexBlockIndex() == dummyEndNode
					.getVertexBlockIndex()))
				blockIds.add(tempNode.getVertexBlockIndex());
			if (tempNode.isHasExitOrReturnStatement())
				return blockIds;
			previousEdgeWeight = -1;
			for (CustomWeightedEdge customWeightedEdge : directedWeightedPseudograph
					.outgoingEdgesOf(tempNode)) {
				edgeWeight = (int) directedWeightedPseudograph
						.getEdgeWeight(customWeightedEdge);
				if (edgeWeight <= ballLarusIdentifier
						&& edgeWeight > previousEdgeWeight) {
					previousEdgeWeight = edgeWeight;
					tempCustomWeightedEdge = customWeightedEdge;
				}
			}
			tempNode = directedWeightedPseudograph
					.getEdgeTarget(tempCustomWeightedEdge);
			ballLarusIdentifier -= ((int) directedWeightedPseudograph
					.getEdgeWeight(tempCustomWeightedEdge));
		}

		return blockIds;
	}

	private static DAGGraph getDAGGraphForBLIdentifier(
			List<DAGGraph> dagGraphList, int ballLarusIdentifier) {
		// TODO Auto-generated method stub

		for (DAGGraph dagGraph : dagGraphList) {
			if (ballLarusIdentifier >= dagGraph.getStartBallLarusIdentifier()
					&& ballLarusIdentifier < dagGraph
							.getEndBallLarusIdentifier())
				return dagGraph;
		}
		return null;
	}
}
