package e0210.BasicBlockSequenceTrace;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import e0210.soot.ThreadLocalMethodTupleBean;

public class ThreadTraceWalk {

	public List<ThreadLocalMethodTupleBean> threadLocalMethodTupleBeans = new ArrayList<ThreadLocalMethodTupleBean>();
	public HashMap<String, List<ThreadLocalMethodTupleBean>> orderedThreadLocalMethodTuple = new HashMap<String, List<ThreadLocalMethodTupleBean>>();

	public ThreadLocalMethodTupleBean addThreadLocalMethodTuple(
			String methodName, String threadId,
			String methodInvocationNumberAmongThisMethodCalls,
			String ballLarusIdentificationNumber,
			String methodInvocationNumberAmongThisThreadCalls) {
		// TODO Auto-generated method stub
		ThreadLocalMethodTupleBean threadLocalMethodTupleBean = new ThreadLocalMethodTupleBean(
				methodName.hashCode(), methodName, threadId,
				Integer.parseInt(methodInvocationNumberAmongThisMethodCalls),
				Integer.parseInt(methodInvocationNumberAmongThisThreadCalls),
				Integer.parseInt(ballLarusIdentificationNumber));
		threadLocalMethodTupleBeans.add(threadLocalMethodTupleBean);
		return threadLocalMethodTupleBean;
	}

	public void createOrderedThreadLocalMethodTuple() {
		for (ThreadLocalMethodTupleBean threadLocalMethodTupleBean : threadLocalMethodTupleBeans) {
			if (!orderedThreadLocalMethodTuple
					.containsKey(threadLocalMethodTupleBean.threadId)) {
				List<ThreadLocalMethodTupleBean> threadLocalMethodTupleBeans = new ArrayList<ThreadLocalMethodTupleBean>();
				threadLocalMethodTupleBeans.add(threadLocalMethodTupleBean);
				orderedThreadLocalMethodTuple.put(
						threadLocalMethodTupleBean.threadId,
						threadLocalMethodTupleBeans);
			} else {
				List<ThreadLocalMethodTupleBean> threadLocalMethodTupleBeans = orderedThreadLocalMethodTuple
						.get(threadLocalMethodTupleBean.threadId);
				threadLocalMethodTupleBeans.add(threadLocalMethodTupleBean);
				orderedThreadLocalMethodTuple.put(
						threadLocalMethodTupleBean.threadId,
						threadLocalMethodTupleBeans);
			}
		}

		ThreadLocalMethodTupleBean dummyThreadLocalMethodTupleBean = new ThreadLocalMethodTupleBean();
		for (String threadId : orderedThreadLocalMethodTuple.keySet()) {
			List<ThreadLocalMethodTupleBean> threadLocalMethodTupleBeans = orderedThreadLocalMethodTuple
					.get(threadId);
			List<ThreadLocalMethodTupleBean> sortedThreadLocalMethodTupleBeans = new ArrayList<ThreadLocalMethodTupleBean>(Collections.nCopies(threadLocalMethodTupleBeans.size(),
			  dummyThreadLocalMethodTupleBean));
			 
			for (ThreadLocalMethodTupleBean threadLocalMethodTupleBean : threadLocalMethodTupleBeans)
				sortedThreadLocalMethodTupleBeans
						.set(threadLocalMethodTupleBean.methodInvocationNumberAmongThisThreadCalls,
								threadLocalMethodTupleBean);
			orderedThreadLocalMethodTuple.put(threadId,
					sortedThreadLocalMethodTupleBeans);
		}

	}

	public String getOrderedThreadLocalMethodTupleString() {
		// TODO Auto-generated method stub
		String orderedThreadLocalMethodTupleString = "";
		for (String threadId : orderedThreadLocalMethodTuple.keySet()) {
			orderedThreadLocalMethodTupleString += "ThreadID: " + threadId;
			orderedThreadLocalMethodTupleString += "\n";
			for (ThreadLocalMethodTupleBean threadLocalMethodTupleBean : orderedThreadLocalMethodTuple
					.get(threadId)) {
				orderedThreadLocalMethodTupleString += threadLocalMethodTupleBean
						.toTupleString();
			}
		}
		return orderedThreadLocalMethodTupleString.toString();
	}
}
