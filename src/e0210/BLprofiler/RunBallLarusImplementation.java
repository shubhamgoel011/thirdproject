package e0210.BLprofiler;

import java.util.HashMap;
import org.jgrapht.graph.DirectedWeightedPseudograph;

import soot.Body;
import soot.Local;
import soot.LongType;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;
import e0210.Utilities.ConstantUtils;
import e0210.Utilities.SootUtilities;
import e0210.Utilities.Utilities;
import e0210.graph.CustomWeightedEdge;
import e0210.graph.GraphUtilities;

public class RunBallLarusImplementation {

	private static long ballLarusIdentifierGlobalCount = 0;

	public static Local runBallLarus(Body body,
			Local methodInvocationNumberByThisThread,
			Local numOfTimesThisMethodCalledByThisThread) {
		ExceptionalBlockGraph exceptionalBlockGraph = new ExceptionalBlockGraph(
				body);

		// synchronized (this) {
		// System.out.println(body.toString());
		// CFGToDotGraph dotGraph = new CFGToDotGraph();
		// DotGraph doGraph2 = dotGraph.drawCFG(exceptionalBlockGraph);
		// doGraph2.plot(body.getMethod().getDeclaringClass().getName()
		// + body.getMethod().getName() + "CFG1"+".dot");
		// }

		/*
		 * It creates DirectedWeightedPseudograph by adding dummy start and end
		 * node
		 */
		DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph = GraphUtilities
				.getDirectedWeightedPseudograph(exceptionalBlockGraph, body);

		/*
		 * Exporting graph Initially i.e before instrumentation or applying BL
		 * Id
		 */

		/*
		 * GraphUtilities.convertGraphtoDOT(body.getMethod().getDeclaringClass()
		 * .getName() + body.getMethod().getName() + "CFGInitial",
		 * directedWeightedPseudograph);
		 */

		/* Applying BL profiler algorithm to return no of paths from each Block */
		HashMap<Block, Integer> numOfPathsFromBlock = BallLarusProfiler
				.profile(exceptionalBlockGraph.size(),
						directedWeightedPseudograph);

		// Utilities.printNumOfPathsFromBlock(body, numOfPathsFromBlock);

		/* It returns no of paths originating from dummy Start node */
		int numOfPathsInMethod = Utilities
				.getNumOfPathsInMethod(numOfPathsFromBlock);

		/* Creating Local BL Identifier in each method */
		Local methodBallLarusIdentifier = Utilities.createLocal(
				ConstantUtils.BALL_LARUS_PATH_IDENTIFIER, LongType.v(), body);

		long threadLocalBallLarusIdentifierGlobalCount;

		synchronized (RunBallLarusImplementation.class) {
			threadLocalBallLarusIdentifierGlobalCount = ballLarusIdentifierGlobalCount;
			ballLarusIdentifierGlobalCount += numOfPathsInMethod;

			Utilities.storeDAGToFile(threadLocalBallLarusIdentifierGlobalCount,
					threadLocalBallLarusIdentifierGlobalCount
							+ numOfPathsInMethod, directedWeightedPseudograph);
		}

		SootUtilities.initializeMethodBallLarusIdentifier(body,
				methodBallLarusIdentifier,
				threadLocalBallLarusIdentifierGlobalCount);

		BallLarusProfiler.instrument(directedWeightedPseudograph, body,
				methodBallLarusIdentifier,
				threadLocalBallLarusIdentifierGlobalCount,
				methodInvocationNumberByThisThread,
				numOfTimesThisMethodCalledByThisThread);

		/*
		 * PatchingChain<Unit> units = body.getUnits(); Iterator<Unit>
		 * statementIterator = units.snapshotIterator();
		 * 
		 * while (statementIterator.hasNext()) { Stmt statement = (Stmt)
		 * statementIterator.next();
		 * 
		 * if (statement instanceof ReturnStmt || statement instanceof
		 * ReturnVoidStmt || Utilities.isExitStatement(statement))
		 * SootUtilities.printMethodBallLarusPrintStatement(body, statement,
		 * methodBallLarusIdentifier);
		 * 
		 * }
		 */

		/*
		 * GraphUtilities.convertGraphtoDOT(body.getMethod().getDeclaringClass()
		 * .getName() + body.getMethod().getName() + "CFG",
		 * directedWeightedPseudograph);
		 */

		return methodBallLarusIdentifier;
	}

}
