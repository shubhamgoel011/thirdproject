package e0210.BLprofiler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.jgrapht.graph.DirectedWeightedPseudograph;
import org.jgrapht.traverse.TopologicalOrderIterator;

import e0210.Utilities.SootUtilities;
import e0210.graph.BackEdgeFinder;
import e0210.graph.CustomWeightedEdge;
import soot.Body;
import soot.Local;
import soot.PatchingChain;
import soot.Unit;
import soot.jimple.AssignStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.Jimple;
import soot.jimple.LongConstant;
import soot.jimple.Stmt;
import soot.toolkits.graph.Block;

public class BallLarusProfiler {

	/* It returns no of paths in graph by applying BL algorithm from each node */
	public static HashMap<Block, Integer> profile(
			int numOfBlocksInExceptionalBlockGraph,
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph) {

		/*
		 * Converts CFG to DAG i.e. remove BackEdges and for each backEdge add a
		 * node from dummy start node to backEdge target and from backEdge
		 * source to dummy End node
		 */
		convertCFGtoDAG(numOfBlocksInExceptionalBlockGraph,
				directedWeightedPseudograph);

		/* Getting Topological sort of the vertices/Blocks */
		TopologicalOrderIterator<Block, CustomWeightedEdge> topologicalOrderIterator = new TopologicalOrderIterator<Block, CustomWeightedEdge>(
				directedWeightedPseudograph);
		List<Block> topologicalSortedBlockList = new LinkedList<Block>();
		while (topologicalOrderIterator.hasNext()) {
			topologicalSortedBlockList.add(topologicalOrderIterator.next());
		}

		/* Getting reverse Topological order */
		Collections.reverse(topologicalSortedBlockList);

		HashMap<Block, Integer> numOfPathsFromBlock = new HashMap<Block, Integer>();

		/* Applying BL algorithm to return no of Paths from from each block */
		for (Block block : topologicalSortedBlockList) {
			if (block.getIndexInMethod() == numOfBlocksInExceptionalBlockGraph)
				numOfPathsFromBlock.put(block, 1);
			else {
				numOfPathsFromBlock.put(block, 0);
				for (Block target : block.getSuccs()) {
					Set<CustomWeightedEdge> customWeightedEdges = directedWeightedPseudograph
							.getAllEdges(block, target);
					for (CustomWeightedEdge customWeightedEdge : customWeightedEdges) {
						directedWeightedPseudograph.setEdgeWeight(
								customWeightedEdge,
								numOfPathsFromBlock.get(block));
						numOfPathsFromBlock.put(block,
								numOfPathsFromBlock.get(block)
										+ numOfPathsFromBlock.get(target));
					}
				}
			}
		}
		return numOfPathsFromBlock;
	}

	/*
	 * Converts CFG to DAG i.e. remove BackEdges and for each backEdge add a
	 * node from dummy start node to backEdge target and from backEdge source to
	 * dummy End node
	 */
	private static void convertCFGtoDAG(
			int numOfBlocksInExceptionalBlockGraph,
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph) {
		// TODO Auto-generated method stub
		Block dummyStartBlock = null, dummyEndBlock = null;

		/* Finding start and end node for this CFG */
		for (Block source : directedWeightedPseudograph.vertexSet()) {
			if (source.getIndexInMethod() == -1)
				dummyStartBlock = source;
			else if (source.getIndexInMethod() == numOfBlocksInExceptionalBlockGraph)
				dummyEndBlock = source;

			/*
			 * int sourceIndex = source.getIndexInMethod();
			 * 
			 * for (Block target : source.getSuccs()) { if (sourceIndex >=
			 * target.getIndexInMethod()) {
			 * backEdges.add(directedWeightedPseudograph.getEdge(source,
			 * target)); } }
			 */
		}

		BackEdgeFinder backEdgeFinder = new BackEdgeFinder(
				directedWeightedPseudograph.vertexSet().size());

		/* Finding backedges in CFG */
		backEdgeFinder.find(directedWeightedPseudograph, dummyStartBlock);
		List<CustomWeightedEdge> backEdges = backEdgeFinder.getBackEdges();

		Block source, target;
		for (CustomWeightedEdge backEdge : backEdges) {
			/* Getting backEdge - source and target */
			source = directedWeightedPseudograph.getEdgeSource(backEdge);
			target = directedWeightedPseudograph.getEdgeTarget(backEdge);

			/*
			 * Adding backEdge component 1 from dummy start node to backEdge
			 * target
			 */
			CustomWeightedEdge backEdge1 = directedWeightedPseudograph.addEdge(
					dummyStartBlock, target);
			backEdge1.setEdgeType(CustomWeightedEdge.BACK_EDGE);
			backEdge1.setBackEdgeSource(source);
			backEdge1.setBackEdgetarget(target);

			/*
			 * Adding backEdge component 2 from backEdge source to dummy end
			 * node
			 */
			CustomWeightedEdge backEdge2 = directedWeightedPseudograph.addEdge(
					source, dummyEndBlock);
			backEdge2.setEdgeType(CustomWeightedEdge.BACK_EDGE);
			backEdge2.setBackEdgeSource(source);
			backEdge2.setBackEdgetarget(target);

			/* Removing backEdge from graph */
			directedWeightedPseudograph.removeEdge(backEdge);

			/* Setting backEdge target in the list of dummy Start node successor */
			List<Block> startBlockSuccs = getModifiableList(dummyStartBlock
					.getSuccs());
			startBlockSuccs.add(target);
			dummyStartBlock.setSuccs(startBlockSuccs);

			/*
			 * Setting backEdge source in the list of dummy End node
			 * Predecessors
			 */
			List<Block> endBlockPreds = getModifiableList(dummyEndBlock
					.getPreds());
			endBlockPreds.add(source);
			dummyEndBlock.setPreds(endBlockPreds);

			/* Changing backEdge Source successor list */
			List<Block> sourceBackEdgeBlockSuccs = getModifiableList(source
					.getSuccs());
			sourceBackEdgeBlockSuccs.remove(target);
			sourceBackEdgeBlockSuccs.add(dummyEndBlock);
			source.setSuccs(sourceBackEdgeBlockSuccs);

			/* Changing backEdge Target predecessor list */
			List<Block> targetBackEdgeBlockPreds = getModifiableList(target
					.getPreds());
			targetBackEdgeBlockPreds.remove(source);
			targetBackEdgeBlockPreds.add(dummyStartBlock);
			target.setPreds(targetBackEdgeBlockPreds);

		}
	}

	/*
	 * It just returns a new Arraylist for the given input arraylist so that it
	 * can be modifiable
	 */
	private static ArrayList<Block> getModifiableList(
			List<Block> unmodifiableBlockList) {
		// TODO Auto-generated method stub

		ArrayList<Block> blockList = new ArrayList<Block>();
		for (Block block : unmodifiableBlockList)
			blockList.add(block);
		return blockList;
	}

	public static void instrument(
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph,
			Body body, Local methodBallLarusIdentifier,
			long ballLarusIdentifierGlobalCount,
			Local methodInvocationNumberByThisThread,
			Local numOfTimesThisMethodCalledByThisThread) {
		// TODO Auto-generated method stub
		LinkedList<CustomWeightedEdge> backEdges = new LinkedList<CustomWeightedEdge>();
		for (CustomWeightedEdge edge : directedWeightedPseudograph.edgeSet()) {

			if (edge.getEdgeType() == CustomWeightedEdge.NORMAL_EDGE) {
				try {
					body.getUnits().insertOnEdge(
							getMethodBallLarusIdentifierIncreaseStatement(
									methodBallLarusIdentifier,
									(long) directedWeightedPseudograph
											.getEdgeWeight(edge)),
							directedWeightedPseudograph.getEdgeSource(edge)
									.getTail(),
							directedWeightedPseudograph.getEdgeTarget(edge)
									.getHead());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					instrumnetForExceptionalEdge(
							getMethodBallLarusIdentifierIncreaseStatement(
									methodBallLarusIdentifier,
									(long) directedWeightedPseudograph
											.getEdgeWeight(edge)),
							directedWeightedPseudograph.getEdgeSource(edge),
							directedWeightedPseudograph.getEdgeTarget(edge),
							directedWeightedPseudograph, body);

				}
			} else if (edge.getEdgeType() == CustomWeightedEdge.DUMMY_START_EDGE)
				instrumentForDummyStartEdge(body, methodBallLarusIdentifier,
						(long) directedWeightedPseudograph.getEdgeWeight(edge));
			else if (edge.getEdgeType() == CustomWeightedEdge.BACK_EDGE)
				backEdges.add(edge);
		}
		instrumentForBackEdge(backEdges, directedWeightedPseudograph, body,
				methodBallLarusIdentifier, ballLarusIdentifierGlobalCount,
				methodInvocationNumberByThisThread,
				numOfTimesThisMethodCalledByThisThread);
		return;
	}

	private static void instrumnetForExceptionalEdge(
			Unit methodBallLarusIdentifierIncreaseStatement,
			Block edgeSource,
			Block edgeTarget,
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph,
			Body body) {
		// TODO Auto-generated method stub

		/*
		 * Chain<Trap> trapList = body.getTraps();
		 * 
		 * Jimple.v().newca body.getUnits().
		 * 
		 * for (Trap trap : trapList){ //trap.
		 * System.out.println(trap.getBeginUnit().toString() + "   " +
		 * trap.getEndUnit().toString() + "  " +
		 * trap.getException().toString()+"   "
		 * +trap.getHandlerUnit().toString()); }
		 */

	}

	private static void instrumentForBackEdge(
			LinkedList<CustomWeightedEdge> backEdges,
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph,
			Body body, Local methodBallLarusIdentifier,
			long ballLarusIdentifierGlobalCount,
			Local methodInvocationNumberByThisThread,
			Local numOfTimesThisMethodCalledByThisThread) {
		// TODO Auto-generated method stub
		while (!backEdges.isEmpty()) {
			Iterator<CustomWeightedEdge> iterator = backEdges.iterator();
			CustomWeightedEdge backEdge1 = null, backEdge2 = null;
			if (iterator.hasNext()) {
				backEdge1 = iterator.next();
				iterator.remove();
			}

			while (iterator.hasNext() && backEdge1 != null) {
				backEdge2 = iterator.next();
				if (backEdge2.getBackEdgeSource().getIndexInMethod() == backEdge1
						.getBackEdgeSource().getIndexInMethod()
						&& backEdge2.getBackEdgetarget().getIndexInMethod() == backEdge1
								.getBackEdgetarget().getIndexInMethod()) {

					iterator.remove();
					break;
				}
			}

			if (backEdge1 != null && backEdge2 != null) {
				try {

					body.getUnits()
							.insertOnEdge(
									getUnitsToPrintAndInitialiazeMethodBallLarusIdentifier(
											methodBallLarusIdentifier,
											getBackEdgeWeight(
													directedWeightedPseudograph,
													backEdge1, backEdge2, true),
											getBackEdgeWeight(
													directedWeightedPseudograph,
													backEdge1, backEdge2, false),
											ballLarusIdentifierGlobalCount,
											body,
											methodInvocationNumberByThisThread,
											numOfTimesThisMethodCalledByThisThread),
									backEdge1.getBackEdgeSource().getTail(),
									backEdge2.getBackEdgetarget().getHead());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					/*System.out.println(backEdge1.getBackEdgeSource().getTail()
							+ "  " + backEdge2.getBackEdgetarget().getHead());*/
				}
			}
			/*
			 * Write code for removing corresponding two edges to backedge and
			 * add backedge and set weight on that backedge and change successor
			 * and predecessor. But Don't write it otherwise in
			 * processedOutput.java you won't be able to backtrack the block ID
			 */

		}
	}

	private static long getBackEdgeWeight(
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph,
			CustomWeightedEdge backEdge1, CustomWeightedEdge backEdge2,
			boolean isStartBackEdge) {
		// TODO Auto-generated method stub
		if (isStartBackEdge) {
			if (directedWeightedPseudograph.getEdgeSource(backEdge1)
					.getIndexInMethod() == -1)
				return (long) directedWeightedPseudograph
						.getEdgeWeight(backEdge1);
			else
				return (long) directedWeightedPseudograph
						.getEdgeWeight(backEdge2);
		} else {
			if (directedWeightedPseudograph.getEdgeSource(backEdge1)
					.getIndexInMethod() == -1)
				return (long) directedWeightedPseudograph
						.getEdgeWeight(backEdge2);
			else
				return (long) directedWeightedPseudograph
						.getEdgeWeight(backEdge1);

		}
	}

	private static List<Unit> getUnitsToPrintAndInitialiazeMethodBallLarusIdentifier(
			Local methodBallLarusIdentifier, long backEdgeValue1,
			long backEdgeValue2, long ballLarusIdentifierGlobalCount,
			Body body, Local methodInvocationNumberByThisThread,
			Local numOfTimesThisMethodCalledByThisThread) {
		// TODO Auto-generated method stub
		List<Unit> units = new ArrayList<Unit>();
		units.add(getMethodBallLarusIdentifierIncreaseStatement(
				methodBallLarusIdentifier, backEdgeValue2));
		units.add(SootUtilities.getThreadTupleOfThisMethod(body,
				methodInvocationNumberByThisThread,
				numOfTimesThisMethodCalledByThisThread,
				methodBallLarusIdentifier));
		units.add(getMethodBallLarusIdentifierInitializationStatement(
				methodBallLarusIdentifier, backEdgeValue1
						+ ballLarusIdentifierGlobalCount));
		return units;
	}

	private static Unit getMethodBallLarusIdentifierIncreaseStatement(
			Local methodBallLarusIdentifier, Long value) {
		// TODO Auto-generated method stub
		AssignStmt assignStmt = Jimple.v().newAssignStmt(
				methodBallLarusIdentifier,
				Jimple.v().newAddExpr(methodBallLarusIdentifier,
						LongConstant.v(value)));
		return assignStmt;
	}

	private static Unit getMethodBallLarusIdentifierInitializationStatement(
			Local methodBallLarusIdentifier, Long value) {
		// TODO Auto-generated method stub
		AssignStmt assignStmt = Jimple.v().newAssignStmt(
				methodBallLarusIdentifier, LongConstant.v(value));
		return assignStmt;
	}

	private static void instrumentForDummyStartEdge(Body body,
			Local methodBallLarusIdentifier, long value) {
		// TODO Auto-generated method stub

		PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> statementIterator = units.snapshotIterator();
		while (statementIterator.hasNext()) {
			Stmt statement = (Stmt) statementIterator.next();
			if (!(statement instanceof IdentityStmt)) {
				AssignStmt increaseMethodBallLarusIdentifierStatement = Jimple
						.v().newAssignStmt(
								methodBallLarusIdentifier,
								Jimple.v().newAddExpr(
										methodBallLarusIdentifier,
										LongConstant.v(value)));
				body.getUnits().insertAfter(
						increaseMethodBallLarusIdentifierStatement, statement);
				return;
			}
		}
		return;

	}

}