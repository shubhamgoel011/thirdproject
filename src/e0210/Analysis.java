package e0210;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import e0210.BLprofiler.RunBallLarusImplementation;
import e0210.Utilities.ConstantUtils;
import e0210.Utilities.SootUtilities;
import e0210.Utilities.Utilities;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.LongType;
import soot.toolkits.graph.Block;

public class Analysis extends BodyTransformer {

	// public static long ballLarusIdentifierGlobalCount = 0;

	/*
	 * Creating a Directory and File if it is not already created. If file
	 * exists then delete it.
	 */
	static {
		File dirFile = new File(ConstantUtils.DIRECTORY_NAME);
		if (dirFile.exists()) {
			File file = new File(dirFile,
					ConstantUtils.DAG_GRAPH_SERIALIZABLE_FILE);
			if (file.exists())
				file.delete();

		} else
			dirFile.mkdir();
		File file = new File(dirFile, ConstantUtils.DAG_GRAPH_SERIALIZABLE_FILE);
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	@Override
	protected void internalTransform(Body body, String phaseName,
			Map<String, String> options) {

		/* Not instrumenting the Helper Class */
		if (Utilities.isHelperClass(body.getMethod().getDeclaringClass()
				.getName()))
			return;

		if (!body.getMethod().getDeclaringClass().toString().contains("test"))
			return;

		/* Creating Local Method Invocation Count By This Thread in each method */
		Local methodInvocationNumberByThisThread = Utilities.createLocal(
				ConstantUtils.METHOD_INVOCATION_NUMBER_BY_THIS_THREAD,
				LongType.v(), body);

		Local numOfTimesThisMethodCalledByThisThread = Utilities
				.createLocal(
						ConstantUtils.NUMBER_OF_TIMES_THIS_METHOD_CALLED_BY_THIS_THREAD,
						LongType.v(), body);

		Local childThreadId = Utilities.createLocal(
				ConstantUtils.CHILD_THREAD_ID, LongType.v(), body);

		Local methodBallLarusIdentifier = RunBallLarusImplementation
				.runBallLarus(body, methodInvocationNumberByThisThread,
						numOfTimesThisMethodCalledByThisThread);

		SootUtilities.initializeMethodInvocationNumberByThisThread(body,
				methodInvocationNumberByThisThread,
				numOfTimesThisMethodCalledByThisThread);
		SootUtilities.newThreadInvocationInThisMethod(body, childThreadId);
		SootUtilities.printThreadTupleOfThisMethod(body,
				methodInvocationNumberByThisThread,
				numOfTimesThisMethodCalledByThisThread,
				methodBallLarusIdentifier);

		/*
		 * //if(body.getMethod().getName().contains("run")){ synchronized (this)
		 * { ExceptionalBlockGraph exceptionalBlockGraph = new
		 * ExceptionalBlockGraph( body); for (Block block :
		 * exceptionalBlockGraph.getBlocks())
		 * System.out.println(block.toString()); } synchronized (this) {
		 * System.out.println(body.toString()); }//}
		 */
	
	}

	public void finish(String testcase) {

		DirectedPseudograph<Block, DefaultEdge> graph = new DirectedPseudograph<Block, DefaultEdge>(
				DefaultEdge.class);
		VertexNameProvider<Block> id = new VertexNameProvider<Block>() {

			@Override
			public String getVertexName(Block b) {
				return String.valueOf("\""
						+ b.getBody().getMethod().getNumber() + " "
						+ b.getIndexInMethod() + "\"");
			}
		};

		VertexNameProvider<Block> name = new VertexNameProvider<Block>() {

			@Override
			public String getVertexName(Block b) {
				String body = b.toString().replace("\'", "").replace("\"", "");
				return body;
			}
		};

		new File("sootOutput").mkdir();

		DOTExporter<Block, DefaultEdge> exporter = new DOTExporter<Block, DefaultEdge>(
				id, name, null);
		try {
			exporter.export(new PrintWriter("sootOutput/" + testcase + ".dot"),
					graph);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return;
	}

}