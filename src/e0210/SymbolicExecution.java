package e0210;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.IntExpr;

import e0210.Utilities.ConstantUtils;
import e0210.Utilities.Utilities;
import e0210.Utilities.bean.CriticalEventTypes;
import e0210.Utilities.bean.ThreadLocalTupleBean;
import e0210.soot.ThreadLocalMethodTupleBean;
import soot.Body;
import soot.Local;
import soot.Scene;
import soot.SceneTransformer;
import soot.Unit;
import soot.Value;
import soot.jimple.AddExpr;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.ConditionExpr;
import soot.jimple.Constant;
import soot.jimple.DivExpr;
import soot.jimple.EqExpr;
import soot.jimple.GeExpr;
import soot.jimple.GtExpr;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.InterfaceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.LeExpr;
import soot.jimple.LtExpr;
import soot.jimple.MulExpr;
import soot.jimple.NeExpr;
import soot.jimple.RemExpr;
import soot.jimple.ReturnStmt;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.SubExpr;
import soot.jimple.VirtualInvokeExpr;
import soot.jimple.XorExpr;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;

public class SymbolicExecution extends SceneTransformer {

	String project;
	String testcase;
	HashMap<String, List<ThreadLocalMethodTupleBean>> orderedThreadLocalMethodTuple;
	Context context;

	public static HashMap<String, List<ThreadLocalTupleBean>> intraThreadTrace; // =
	// new
	// HashMap<String,
	// List<ThreadLocalTupleBean>>();

	// public static List<BoolExpr> programOrderConstraints = new
	// ArrayList<BoolExpr>();
	public static List<BoolExpr> pathConstraints = new ArrayList<BoolExpr>();

	private String symbolicMethodReturn = "";

	// Thread Local Variables
	HashMap<String, String> forkJoinThreadId = new HashMap<String, String>();
	int numOfForksInThisThread = 0;
	int symbolicSharedThreadGlobalReadCounter = 0;
	int symbolicSharedThreadGlobalWriteCounter = 0;
	int methodNumber = 0;
	int symbolicThreadLocalWriteCounter = 0;

	public SymbolicExecution(
			String project,
			String testcase,
			HashMap<String, List<ThreadLocalMethodTupleBean>> orderedThreadLocalMethodTuple, Context context) {
		super();
		this.project = project;
		this.testcase = testcase;
		this.orderedThreadLocalMethodTuple = orderedThreadLocalMethodTuple;
		this.context = context;
	}

	@Override
	protected void internalTransform(String phaseName,
			Map<String, String> options) {

		/*
		 * SceneTransformer vs BodyTransformer
		 * =================================== BodyTransformer is applied on all
		 * methods separately. It uses multiple worker threads to process the
		 * methods in parallel. SceneTransformer is applied on the whole program
		 * in one go. SceneTransformer uses only one worker thread.
		 * 
		 * It is better to use SceneTransformer for this part of the project
		 * because: 1. During symbolic execution you may need to 'jump' from one
		 * method to another when you encounter a call instruction. This is
		 * easily done in SceneTransformer (see below) 2. There is only one
		 * worker thread in SceneTransformer which saves you from having to
		 * synchronize accesses to any global data structures that you'll use,
		 * (eg data structures to store constraints, trace, etc)
		 * 
		 * How to get method body? ======================= Use
		 * Scene.v().getApplicationClasses() to get all classes and
		 * SootClass::getMethods() to get all methods in a particular class. You
		 * can search these lists to find the body of any method.
		 */

		intraThreadTrace = generateIntraThreadTrace(
				orderedThreadLocalMethodTuple, testcase);

		/*
		 * Perform the following for each thread T: 1. Start with the first
		 * method of thread T. If T is the main thread, the first method is
		 * main(), else it is the run() method of the thread's class 2. Using
		 * (only) the tuples for thread T, walk through the code to reproduce
		 * the path followed by T. If thread T performs method calls then,
		 * during this walk you'll need to jump from one method's body to
		 * another to imitate the flow of control (as discussed during the
		 * Project session) 3. While walking through the code, collect the
		 * intra-thread trace for T and the path constraints. Don't forget that
		 * you need to renumber each dynamic instance of a static variable (i.e.
		 * SSA)
		 */
		return;
	}

	private HashMap<String, List<ThreadLocalTupleBean>> generateIntraThreadTrace(
			HashMap<String, List<ThreadLocalMethodTupleBean>> orderedThreadLocalMethodTuple,
			String testCase) {
		// TODO Auto-generated method stub
		HashMap<String, List<ThreadLocalTupleBean>> intraThreadTrace = new HashMap<String, List<ThreadLocalTupleBean>>();

		// For Each Thread
		for (String threadId : orderedThreadLocalMethodTuple.keySet()) {
			forkJoinThreadId = new HashMap<String, String>();
			numOfForksInThisThread = 0;
			methodNumber = 0;
			symbolicSharedThreadGlobalReadCounter = 0;
			symbolicSharedThreadGlobalWriteCounter = 0;
			symbolicMethodReturn="";
			symbolicThreadLocalWriteCounter = 0;

			List<ThreadLocalTupleBean> threadLocalTupleBeans = new ArrayList<ThreadLocalTupleBean>();
			threadLocalTupleBeans.add(new ThreadLocalTupleBean(threadId,
					CriticalEventTypes.BEGIN));
			intraThreadTrace.put(threadId, threadLocalTupleBeans);

			// For Each Tuple/Method in Thread
			generateThreadSpecificTrace(intraThreadTrace, threadId, testCase,
					null);

			List<ThreadLocalTupleBean> tempThreadLocalTupleBeans = intraThreadTrace
					.get(threadId);
			tempThreadLocalTupleBeans.add(new ThreadLocalTupleBean(threadId,
					CriticalEventTypes.END));
			intraThreadTrace.put(threadId, tempThreadLocalTupleBeans);
		}
		return intraThreadTrace;
	}

	private void generateThreadSpecificTrace(
			HashMap<String, List<ThreadLocalTupleBean>> intraThreadTrace,
			String threadId, String testCase,
			List<String> symbolicMethodLocalParametrs) {
		// TODO Auto-generated method stub

		HashMap<String, String> symbolicMethodLocalWrite = new HashMap<String, String>();

		ThreadLocalMethodTupleBean threadLocalMethodTupleBean = orderedThreadLocalMethodTuple
				.get(threadId).get(methodNumber);
		Body body = Scene.v().getMethod(threadLocalMethodTupleBean.methodName)
				.getActiveBody();
		ExceptionalBlockGraph exceptionalBlockGraph = new ExceptionalBlockGraph(
				body);
		int nextBlockIdIndex = 0;
		int identityStatementNumber = 0;

		// For Each Block in Method
		for (Integer blockId : threadLocalMethodTupleBean.basicBlockSequenceNumber) {
			Block block = exceptionalBlockGraph.getBlocks().get(blockId);
			Iterator<Unit> units = block.iterator();
			nextBlockIdIndex++;

			// For Each Unit in Block
			while (units.hasNext()) {
				Stmt statement = (Stmt) units.next();

				// InvokeStatement
				if (statement instanceof InvokeStmt) {
					InvokeExpr invokeExpr = statement.getInvokeExpr();

					if (invokeExpr instanceof VirtualInvokeExpr) {
						if (((VirtualInvokeExpr) invokeExpr).getMethod()
								.getSignature()
								.equals(ConstantUtils.THREAD_FORK)) {
							String forkedThreadObjectName = ((Local) ((VirtualInvokeExpr) invokeExpr)
									.getBase()).getName();
							forkJoinThreadId.put(forkedThreadObjectName,
									threadId + "." + numOfForksInThisThread);
							List<ThreadLocalTupleBean> tempThreadLocalTupleBeans = intraThreadTrace
									.get(threadId);
							tempThreadLocalTupleBeans
									.add(new ThreadLocalTupleBean(threadId,
											CriticalEventTypes.FORK, threadId
													+ "."
													+ numOfForksInThisThread));
							intraThreadTrace.put(threadId,
									tempThreadLocalTupleBeans);
							numOfForksInThisThread++;
						} else if (((VirtualInvokeExpr) invokeExpr).getMethod()
								.getSignature()
								.equals(ConstantUtils.THREAD_JOIN)) {
							String joinedThreadObjectName = ((Local) ((VirtualInvokeExpr) invokeExpr)
									.getBase()).getName();
							String joinedThreadId = forkJoinThreadId
									.get(joinedThreadObjectName);
							List<ThreadLocalTupleBean> tempThreadLocalTupleBeans = intraThreadTrace
									.get(threadId);
							tempThreadLocalTupleBeans
									.add(new ThreadLocalTupleBean(threadId,
											CriticalEventTypes.JOIN,
											joinedThreadId));
							intraThreadTrace.put(threadId,
									tempThreadLocalTupleBeans);
						}else if (invokeExpr.getMethod().getSignature()
								.startsWith(ConstantUtils.METHOD_INVOKED)) {
							
							/*String fun = invokeExpr.getMethodRef()
									.toString();*/
							List<String> tempSymbolicMethodLocalParametrs = new ArrayList<String>();
							for (Value value : invokeExpr.getArgs()) {
								if (value instanceof Local)
									tempSymbolicMethodLocalParametrs
											.add(symbolicMethodLocalWrite.get(value
													.toString()));
								else
									tempSymbolicMethodLocalParametrs
											.add(value.toString());
							}
							methodNumber++;
							generateThreadSpecificTrace(
									intraThreadTrace, threadId,
									testCase,
									tempSymbolicMethodLocalParametrs);
						}
					} else if (invokeExpr instanceof InterfaceInvokeExpr) {
						if (((InterfaceInvokeExpr) invokeExpr).getMethod()
								.getSignature()
								.equals(ConstantUtils.THREAD_LOCK)) {
							String lockObjectName = ((Stmt) block
									.getPredOf(statement)).getFieldRef()
									.getField().getName();
							String newLockObjectName = "<" + testCase
									+ ".Main: java.util.concurrent.locks.Lock "
									+ lockObjectName + ">";
							List<ThreadLocalTupleBean> tempThreadLocalTupleBeans = intraThreadTrace
									.get(threadId);
							tempThreadLocalTupleBeans
									.add(new ThreadLocalTupleBean(threadId,
											CriticalEventTypes.LOCK,
											newLockObjectName));
							intraThreadTrace.put(threadId,
									tempThreadLocalTupleBeans);
						} else if (((InterfaceInvokeExpr) invokeExpr)
								.getMethod().getSignature()
								.equals(ConstantUtils.THREAD_UNLOCK)) {
							String lockObjectName = ((Stmt) block
									.getPredOf(statement)).getFieldRef()
									.getField().getName();
							String newLockObjectName = "<" + testCase
									+ ".Main: java.util.concurrent.locks.Lock "
									+ lockObjectName + ">";
							List<ThreadLocalTupleBean> tempThreadLocalTupleBeans = intraThreadTrace
									.get(threadId);
							tempThreadLocalTupleBeans
									.add(new ThreadLocalTupleBean(threadId,
											CriticalEventTypes.UNLOCK,
											newLockObjectName));
							intraThreadTrace.put(threadId,
									tempThreadLocalTupleBeans);
						} else if (invokeExpr.getMethod().getSignature()
								.startsWith(ConstantUtils.METHOD_INVOKED)) {
							/*String fun = invokeExpr.getMethodRef()
									.toString();*/
							List<String> tempSymbolicMethodLocalParametrs = new ArrayList<String>();
							for (Value value : invokeExpr.getArgs()) {
								if (value instanceof Local)
									tempSymbolicMethodLocalParametrs
											.add(symbolicMethodLocalWrite.get(value
													.toString()));
								else
									tempSymbolicMethodLocalParametrs
											.add(value.toString());
							}
							methodNumber++;
							generateThreadSpecificTrace(
									intraThreadTrace, threadId,
									testCase,
									tempSymbolicMethodLocalParametrs);
						}
					}  else if (invokeExpr.getMethod().getSignature()
							.startsWith(ConstantUtils.METHOD_INVOKED)) {
						/*String fun = invokeExpr.getMethodRef()
								.toString();*/
						List<String> tempSymbolicMethodLocalParametrs = new ArrayList<String>();
						for (Value value : invokeExpr.getArgs()) {
							if (value instanceof Local)
								tempSymbolicMethodLocalParametrs
										.add(symbolicMethodLocalWrite.get(value
												.toString()));
							else
								tempSymbolicMethodLocalParametrs
										.add(value.toString());
						}
						methodNumber++;
						generateThreadSpecificTrace(
								intraThreadTrace, threadId,
								testCase,
								tempSymbolicMethodLocalParametrs);
					}
				}
				//If return statement then store symbolic return value
				else if (statement instanceof ReturnStmt) {
					symbolicMethodReturn = symbolicMethodLocalWrite
							.get((((ReturnStmt) statement).getOp()
									.toString()));
				}
				// Indentity Statement Local Variable Write
				else if (statement instanceof IdentityStmt
						&& symbolicMethodLocalParametrs != null && symbolicMethodLocalParametrs.size()>0) {
					String leftVar = ((IdentityStmt) statement).getLeftOp()
							.toString();
					symbolicMethodLocalWrite.put(leftVar, Utilities
							.createSymbolicMethodWrite(threadId, body,
									symbolicThreadLocalWriteCounter));

					if (symbolicMethodLocalParametrs.get(
							identityStatementNumber).matches("[0-9]+"))
						pathConstraints.add(context.mkEq(context
								.mkIntConst(symbolicMethodLocalWrite
										.get(leftVar)), context.mkInt(Integer
								.parseInt(symbolicMethodLocalParametrs
										.get(identityStatementNumber)))));
					else
						pathConstraints.add(context.mkEq(context
								.mkIntConst(symbolicMethodLocalWrite
										.get(leftVar)), context
								.mkIntConst(symbolicMethodLocalParametrs
										.get(identityStatementNumber))));
					identityStatementNumber++;
					symbolicThreadLocalWriteCounter++;
				}
				// Assignment Statement
				else if (statement instanceof AssignStmt) {
					AssignStmt assignStatement = (AssignStmt) statement;
					ArithExpr arithExpr1 = null, arithExpr2 = null;


					// If Left is of Defined Type
					if (Utilities.checkType(assignStatement.getLeftOp()
							.getType())) {
						// Now Checking Right side
						if (assignStatement.getRightOp() instanceof Local) {
							arithExpr2 = context
									.mkIntConst(symbolicMethodLocalWrite
											.get(assignStatement.getRightOp()
													.toString()));
						} else if (assignStatement.getRightOp() instanceof Constant) {
							arithExpr2 = context.mkInt(Integer
									.parseInt(assignStatement.getRightOp()
											.toString()));
						} else if (assignStatement.getRightOp() instanceof BinopExpr) {
							BinopExpr binopExpr = (BinopExpr) assignStatement
									.getRightOp();
							IntExpr intExpr1 = null;
							IntExpr intExpr2 = null;

							if (binopExpr.getOp1() instanceof Local)
								intExpr1 = context
										.mkIntConst(symbolicMethodLocalWrite
												.get(binopExpr.getOp1()
														.toString()));
							else
								intExpr1 = context
										.mkInt(Integer.parseInt(binopExpr
												.getOp1().toString()));

							if (binopExpr.getOp2() instanceof Local)
								intExpr2 = context
										.mkIntConst(symbolicMethodLocalWrite
												.get(binopExpr.getOp2()
														.toString()));
							else
								intExpr2 = context
										.mkInt(Integer.parseInt(binopExpr
												.getOp2().toString()));

							if (binopExpr instanceof AddExpr)
								arithExpr2 = context.mkAdd(intExpr1, intExpr2);
							else if (binopExpr instanceof SubExpr)
								arithExpr2 = context.mkSub(intExpr1, intExpr2);
							else if (binopExpr instanceof MulExpr)
								arithExpr2 = context.mkMul(intExpr1, intExpr2);
							else if (binopExpr instanceof DivExpr)
								arithExpr2 = context.mkDiv(intExpr1, intExpr2);
							else if (binopExpr instanceof RemExpr)
								arithExpr2 = context.mkRem(intExpr1, intExpr2);
							else if (binopExpr instanceof XorExpr)
								arithExpr2 = context.mkBV2Int(context.mkBVXOR(
										context.mkInt2BV(32, intExpr1),
										context.mkInt2BV(32, intExpr2)), true);
						} else if (assignStatement.getRightOp() instanceof InvokeExpr) {

							if (assignStatement.getRightOp() instanceof VirtualInvokeExpr) {
								VirtualInvokeExpr invokeExpr = (VirtualInvokeExpr) assignStatement
										.getRightOp();
								if (assignStatement.getRightOp().toString()
										.contains("Value"))
									arithExpr2 = context
											.mkIntConst(symbolicMethodLocalWrite
													.get(invokeExpr.getBase()
															.toString()));
							} else if (assignStatement.getRightOp() instanceof StaticInvokeExpr) {
								StaticInvokeExpr invokeExpr = (StaticInvokeExpr) assignStatement
										.getRightOp();

								if ((invokeExpr.getArgCount() == 1)
										&& (assignStatement.getRightOp()
												.toString().contains("valueOf"))) {
									if (invokeExpr.getArgs().get(0) instanceof Local)
										arithExpr2 = context
												.mkIntConst(symbolicMethodLocalWrite
														.get(invokeExpr
																.getArgs()
																.get(0)
																.toString()));
									else
										arithExpr2 = context.mkInt(Integer
												.parseInt((invokeExpr.getArgs()
														.get(0).toString())));
								} else if (assignStatement.getRightOp()
										.toString().contains("parseInt")) {
									String arrayIndexStatement = ((AssignStmt) body
											.getUnits().getPredOf(statement))
											.getRightOp().toString();
									int val = 0, index = Integer
											.parseInt(arrayIndexStatement.substring(
													arrayIndexStatement
															.length() - 2,
													arrayIndexStatement
															.length() - 1));
									BufferedReader input = null;
									try {
										input = new BufferedReader(
												new FileReader("Testcases/"
														+ project + "/input/"
														+ testcase));
										val = Integer.parseInt(input.readLine()
												.split(" ")[index]);
									} catch (Exception e1) {
										e1.printStackTrace();
									}
									arithExpr2 = context.mkInt(val);
								} else {
									/*String fun = invokeExpr.getMethodRef()
											.toString();*/
									List<String> tempSymbolicMethodLocalParametrs = new ArrayList<String>();
									for (Value value : invokeExpr.getArgs()) {
										if (value instanceof Local)
											tempSymbolicMethodLocalParametrs
													.add(symbolicMethodLocalWrite.get(value
															.toString()));
										else
											tempSymbolicMethodLocalParametrs
													.add(value.toString());
									}
									methodNumber++;
									generateThreadSpecificTrace(
											intraThreadTrace, threadId,
											testCase,
											tempSymbolicMethodLocalParametrs);
									arithExpr2 = context
											.mkIntConst(symbolicMethodReturn);
								}
							}
						}// Shared Variable Read
						else if (assignStatement.getRightOp() instanceof StaticFieldRef) {
							arithExpr2 = context
									.mkIntConst(Utilities
											.createSymbolicSharedThreadRead(
													threadId,
													((StaticFieldRef) assignStatement
															.getRightOp())
															.getField()
															.getName(),
													symbolicSharedThreadGlobalReadCounter));

							List<ThreadLocalTupleBean> tempThreadLocalTupleBeans = intraThreadTrace
									.get(threadId);
							tempThreadLocalTupleBeans
									.add(new ThreadLocalTupleBean(
											threadId,
											CriticalEventTypes.READ,
											((StaticFieldRef) assignStatement
													.getRightOp()).getField()
													.getSignature(),
											Utilities
													.createSymbolicSharedThreadRead(
															threadId,
															((StaticFieldRef) assignStatement
																	.getRightOp())
																	.getField()
																	.getName(),
															symbolicSharedThreadGlobalReadCounter)));
							intraThreadTrace.put(threadId,
									tempThreadLocalTupleBeans);
							symbolicSharedThreadGlobalReadCounter++;
						}
					}
					// Local Variable Write
					if (assignStatement.getLeftOp() instanceof Local) {
						arithExpr1 = context.mkIntConst(Utilities
								.createSymbolicMethodWrite(threadId, body,
										symbolicThreadLocalWriteCounter));
						symbolicMethodLocalWrite.put(assignStatement
								.getLeftOp().toString(), Utilities
								.createSymbolicMethodWrite(threadId, body,
										symbolicThreadLocalWriteCounter));
						symbolicThreadLocalWriteCounter++;
					}// Shared Variable Write
					else if (assignStatement.getLeftOp() instanceof StaticFieldRef) {
						if (!assignStatement.getLeftOp().getType().toString()
								.contains("lock")) {
							arithExpr1 = context
									.mkIntConst(Utilities
											.createSymbolicSharedThreadWrite(
													threadId,
													((StaticFieldRef) assignStatement
															.getLeftOp())
															.getField()
															.getName(),
													symbolicSharedThreadGlobalWriteCounter));

							List<ThreadLocalTupleBean> tempThreadLocalTupleBeans = intraThreadTrace
									.get(threadId);
							tempThreadLocalTupleBeans
									.add(new ThreadLocalTupleBean(
											threadId,
											CriticalEventTypes.WRITE,
											((StaticFieldRef) assignStatement
													.getLeftOp()).getField()
													.getSignature(),
											Utilities
													.createSymbolicSharedThreadWrite(
															threadId,
															((StaticFieldRef) assignStatement
																	.getLeftOp())
																	.getField()
																	.getName(),
															symbolicSharedThreadGlobalWriteCounter)));
							intraThreadTrace.put(threadId,
									tempThreadLocalTupleBeans);

							symbolicSharedThreadGlobalWriteCounter++;
						}

					}
					if (arithExpr1 != null && arithExpr2 != null)
						pathConstraints.add(context
								.mkEq(arithExpr1, arithExpr2));
				}
				// If Statement
				else if (statement instanceof IfStmt) {
					IfStmt ifStatement = (IfStmt) statement;
					ConditionExpr conditionExpr = (ConditionExpr) ifStatement
							.getCondition();
					ArithExpr arithExpr1, arithExpr2;
					BoolExpr boolExpr = null;

					if (conditionExpr.getOp1() instanceof Local)
						arithExpr1 = context
								.mkIntConst(symbolicMethodLocalWrite
										.get(conditionExpr.getOp1().toString()));
					else
						arithExpr1 = context.mkInt(Integer
								.parseInt(conditionExpr.getOp1().toString()));

					if (conditionExpr.getOp2() instanceof Local)
						arithExpr2 = context
								.mkIntConst(symbolicMethodLocalWrite
										.get(conditionExpr.getOp2().toString()));
					else
						arithExpr2 = context.mkInt(Integer
								.parseInt(conditionExpr.getOp2().toString()));

					if (conditionExpr instanceof GeExpr)
						boolExpr = context.mkGe(arithExpr1, arithExpr2);
					else if (conditionExpr instanceof LeExpr)
						boolExpr = context.mkLe(arithExpr1, arithExpr2);
					else if (conditionExpr instanceof GtExpr)
						boolExpr = context.mkGt(arithExpr1, arithExpr2);
					else if (conditionExpr instanceof LtExpr)
						boolExpr = context.mkLt(arithExpr1, arithExpr2);
					else if (conditionExpr instanceof EqExpr)
						boolExpr = context.mkEq(arithExpr1, arithExpr2);
					else if (conditionExpr instanceof NeExpr)
						boolExpr = context.mkNot(context.mkEq(arithExpr1,
								arithExpr2));

					if (ifStatement
							.getTarget()
							.equals(exceptionalBlockGraph
									.getBlocks()
									.get(threadLocalMethodTupleBean.basicBlockSequenceNumber
											.get(nextBlockIdIndex)).getHead()))
						pathConstraints.add(boolExpr);
					else
						pathConstraints.add(context.mkNot(boolExpr));
				}
			}// For Each Unit in Block

		}// For Each Block in Method
		if (threadLocalMethodTupleBean.methodName.contains("clinit")) {
			methodNumber++;
			generateThreadSpecificTrace(intraThreadTrace, threadId, testCase,
					null);
		}
	}
}
