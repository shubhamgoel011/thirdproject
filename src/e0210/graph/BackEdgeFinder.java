package e0210.graph;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.graph.DirectedWeightedPseudograph;

import soot.toolkits.graph.Block;

public class BackEdgeFinder {
	private boolean[] markedOrVisisted, onStack;
	private List<CustomWeightedEdge> backEdges;

	public BackEdgeFinder(int noOfVerices) {
		super();
		this.markedOrVisisted = new boolean[noOfVerices];
		this.onStack = new boolean[noOfVerices];
		this.backEdges = new ArrayList<CustomWeightedEdge>();
	}

	public List<CustomWeightedEdge> getBackEdges() {
		return backEdges;
	}

	public void find(
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph,
			Block source) {

		markedOrVisisted[source.getIndexInMethod() + 1] = true;
		onStack[source.getIndexInMethod() + 1] = true;

		for (Block successor : source.getSuccs()) {
			if (!markedOrVisisted[successor.getIndexInMethod() + 1])
				find(directedWeightedPseudograph, successor);
			else if (onStack[successor.getIndexInMethod() + 1])
				backEdges.add(directedWeightedPseudograph.getEdge(source,
						successor));
		}
		onStack[source.getIndexInMethod() + 1] = false;
	}

}
