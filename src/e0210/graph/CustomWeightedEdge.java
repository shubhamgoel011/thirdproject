package e0210.graph;

import org.jgrapht.graph.DefaultWeightedEdge;

import soot.toolkits.graph.Block;

public class CustomWeightedEdge extends DefaultWeightedEdge {

	private static final long serialVersionUID = -3760447343569211139L;

	public static final int NORMAL_EDGE = 1;
	public static final int BACK_EDGE = 2;
	public static final int DUMMY_START_EDGE = 3;
	public static final int DUMMY_END_EDGE = 4;

	private int edgeType = CustomWeightedEdge.NORMAL_EDGE;
	private transient Block backEdgeSource;
	private transient Block backEdgetarget;

	public int getEdgeType() {
		return edgeType;
	}

	public void setEdgeType(int edgeType) {
		this.edgeType = edgeType;
	}

	public Block getBackEdgeSource() {
		return backEdgeSource;
	}

	public void setBackEdgeSource(Block backEdgeSource) {
		this.backEdgeSource = backEdgeSource;
	}

	public Block getBackEdgetarget() {
		return backEdgetarget;
	}

	public void setBackEdgetarget(Block backEdgetarget) {
		this.backEdgetarget = backEdgetarget;
	}

}
