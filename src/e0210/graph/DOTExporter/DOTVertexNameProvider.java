package e0210.graph.DOTExporter;

import org.jgrapht.ext.VertexNameProvider;
import soot.toolkits.graph.Block;

public class DOTVertexNameProvider implements VertexNameProvider<Block> {

	@Override
	public String getVertexName(Block block) {
		// TODO Auto-generated method stub
		return String.valueOf(block.getIndexInMethod());
	}

}
