package e0210.graph.DOTExporter;

import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.graph.DirectedWeightedPseudograph;

import e0210.graph.CustomWeightedEdge;
import soot.toolkits.graph.Block;

public class DOTEdgeNameProvider implements
		EdgeNameProvider<CustomWeightedEdge> {
	;

	DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph;

	public DOTEdgeNameProvider(
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph) {
		// TODO Auto-generated constructor stub
		this.directedWeightedPseudograph = directedWeightedPseudograph;
	}

	@Override
	public String getEdgeName(CustomWeightedEdge customWeightedEdge) {
		// TODO Auto-generated method stub
		return String.valueOf(directedWeightedPseudograph
				.getEdgeWeight(customWeightedEdge));
	}

}
