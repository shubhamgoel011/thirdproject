/**
 * 
 */
package e0210.graph.DOTExporter;

import java.util.Iterator;

import org.jgrapht.ext.VertexNameProvider;

import soot.toolkits.graph.Block;

/**
 * @author joker
 * 
 */
public class DOTVertexLabelProvider implements VertexNameProvider<Block> {

	@Override
	public String getVertexName(Block block) {
		// TODO Auto-generated method stub
		String s;
		try {
			s = block.toString();
		} catch (Exception e) {
			// TODO: handle exception
			s = getShortString(block);
		}
		return s;
	}

	private String getShortString(Block block) {

		StringBuffer strBuf = new StringBuffer();
		strBuf.append("Block " + block.getIndexInMethod() + ":"
				+ System.getProperty("line.separator"));
		strBuf.append("[preds: ");
		if (block.getPreds() != null) {
			Iterator<Block> it = block.getPreds().iterator();
			while (it.hasNext()) {
				strBuf.append(it.next().getIndexInMethod() + " ");
			}
		}
		strBuf.append("] [succs: ");
		if (block.getSuccs() != null) {
			Iterator<Block> it = block.getSuccs().iterator();
			while (it.hasNext()) {
				strBuf.append(it.next().getIndexInMethod() + " ");
			}
		}
		strBuf.append("]" + System.getProperty("line.separator"));
		return strBuf.toString();

	}
}