package e0210.graph;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jgrapht.ext.DOTExporter;
import org.jgrapht.graph.DirectedWeightedPseudograph;

import e0210.graph.DOTExporter.DOTEdgeNameProvider;
import e0210.graph.DOTExporter.DOTVertexLabelProvider;
import e0210.graph.DOTExporter.DOTVertexNameProvider;
import soot.Body;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;

public class GraphUtilities {
	
	/*
	 * It takes Exceptional Block Graph as input and returns
	 * DirectedWeightedPseudograph by adding dummy start and end node
	 */
	public static DirectedWeightedPseudograph<Block, CustomWeightedEdge> getDirectedWeightedPseudograph(
			ExceptionalBlockGraph exceptionalBlockGraph, Body body) {

		DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph = new DirectedWeightedPseudograph<Block, CustomWeightedEdge>(
				CustomWeightedEdge.class);
		
		/*Adding Verices*/
		for (Block block : exceptionalBlockGraph.getBlocks()) {
			directedWeightedPseudograph.addVertex(block);
			// System.out.println(block.toString());
		}
		
		
		/*Adding Edges*/
		for (Block block : exceptionalBlockGraph.getBlocks()) {
			/*
			 * for (Block source : block.getPreds())
			 * directedWeightedPseudograph.addEdge(source, block);
			 */
			for (Block target : block.getSuccs())
				directedWeightedPseudograph.addEdge(block, target).setEdgeType(
						CustomWeightedEdge.NORMAL_EDGE);
		}

		/* Adding Dummy start Node */
		Block dummyStartBlock = new Block(null, null, body, -1, 0, null);
		dummyStartBlock.setPreds(new ArrayList<Block>());
		dummyStartBlock.setSuccs(exceptionalBlockGraph.getHeads());
		List<Block> predecessorList = new ArrayList<Block>();
		predecessorList.add(dummyStartBlock);
		directedWeightedPseudograph.addVertex(dummyStartBlock);
		for (Block block : exceptionalBlockGraph.getHeads()) {
			directedWeightedPseudograph.addEdge(dummyStartBlock, block)
					.setEdgeType(CustomWeightedEdge.DUMMY_START_EDGE);
			block.setPreds(predecessorList);
		}

		/* Adding Dummy end Node */
		Block dummyEndBlock = new Block(null, null, body, exceptionalBlockGraph
				.getBlocks().size(), 0, null);
		dummyEndBlock.setSuccs(new ArrayList<Block>());
		dummyEndBlock.setPreds(exceptionalBlockGraph.getTails());
		List<Block> successorList = new ArrayList<Block>();
		successorList.add(dummyEndBlock);
		directedWeightedPseudograph.addVertex(dummyEndBlock);
		for (Block block : exceptionalBlockGraph.getTails()) {
			directedWeightedPseudograph.addEdge(block, dummyEndBlock)
					.setEdgeType(CustomWeightedEdge.DUMMY_END_EDGE);
			block.setSuccs(successorList);
		}

		return directedWeightedPseudograph;
	}

	/* This function export the graph to DOT file */
	public static void convertGraphtoDOT(
			String fileName,
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph) {

		DOTExporter<Block, CustomWeightedEdge> dotExporter = new DOTExporter<Block, CustomWeightedEdge>(
				new DOTVertexNameProvider(), new DOTVertexLabelProvider(),
				new DOTEdgeNameProvider(directedWeightedPseudograph));

		try {
			dotExporter.export(new FileWriter("Output/" + fileName + ".dot"),
					directedWeightedPseudograph);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
