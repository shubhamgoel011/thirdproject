package e0210.Utilities;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.jgrapht.graph.DirectedWeightedPseudograph;

import e0210.Utilities.bean.DAGGraph;
import e0210.Utilities.bean.VertexBlock;
import e0210.graph.CustomWeightedEdge;
import e0210.soot.Helper;
import e0210.soot.ThreadLocalMethodTupleBean;
import soot.Body;
import soot.IntType;
import soot.Local;
import soot.LongType;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.toolkits.graph.Block;
import soot.util.Chain;

public class Utilities {
	
	public static boolean isMainMethod(Body body) {
		// TODO Auto-generated method stub
		return body.getMethod().getSubSignature()
				.equals("void main(java.lang.String[])");
	}

	public static boolean isExitStatement(Stmt statement) {
		// TODO Auto-generated method stub
		if (statement instanceof InvokeStmt) {
			InvokeExpr iexpr = (InvokeExpr) ((InvokeStmt) statement)
					.getInvokeExpr();
			if (iexpr instanceof StaticInvokeExpr) {
				SootMethod target = ((StaticInvokeExpr) iexpr).getMethod();
				if (target.getSignature().equals(
						"<java.lang.System: void exit(int)>")) {
					return true;
				}
			}
		}
		return false;
	}

	public static void printNumOfPathsFromBlock(Body body,
			HashMap<Block, Integer> numOfPathsFromBlock) {
		System.out.println(body.getMethod().getName());
		for (Block block : numOfPathsFromBlock.keySet())
			System.out.println(block.toShortString() + "   "
					+ String.valueOf(numOfPathsFromBlock.get(block)));
		return;
	}

	public static int getNumOfPathsInMethod(
			HashMap<Block, Integer> numOfPathsFromBlock) {
		// TODO Auto-generated method stub
		for (Block block : numOfPathsFromBlock.keySet()) {
			if (block.getIndexInMethod() == -1)
				return numOfPathsFromBlock.get(block);
		}
		return 1;
	}

	public static Local createLocal(String variableName, LongType v, Body body) {
		// TODO Auto-generated method stub
		Local local = Jimple.v().newLocal(variableName, v);
		body.getLocals().add(local);
		return local;
	}
	
	public static Local createLocal(String variableName, IntType v, Body body) {
		// TODO Auto-generated method stub
		Local local = Jimple.v().newLocal(variableName, v);
		body.getLocals().add(local);
		return local;
	}

	public static void storeDAGToFile(
			long startBallLarusIdentifier,
			long endBallLarusIdentifier,
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph) {
		// TODO Auto-generated method stub

		DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> serializableDirectedWeightedPseudograph = getSerializableDirectedWeightedPseudograph(directedWeightedPseudograph);

		List<DAGGraph> dagGraphs = loadDAGFromFile();

		try (FileOutputStream fileOutputStream = new FileOutputStream(
				ConstantUtils.DIRECTORY_NAME + "/"
						+ ConstantUtils.DAG_GRAPH_SERIALIZABLE_FILE);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(
						fileOutputStream)) {
			for (DAGGraph dagGraph : dagGraphs) {
				objectOutputStream.writeObject(dagGraph);
			}

			objectOutputStream.writeObject(new DAGGraph(
					startBallLarusIdentifier, endBallLarusIdentifier,
					serializableDirectedWeightedPseudograph));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private static DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> getSerializableDirectedWeightedPseudograph(
			DirectedWeightedPseudograph<Block, CustomWeightedEdge> directedWeightedPseudograph) {
		// TODO Auto-generated method stub

		DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> serializableDirectedWeightedPseudograph = new DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge>(
				CustomWeightedEdge.class);

		for (Block block : directedWeightedPseudograph.vertexSet())
			serializableDirectedWeightedPseudograph.addVertex(new VertexBlock(
					block.getIndexInMethod(), hasExitOrReturnStatement(block)));

		Set<VertexBlock> vertexBlocks = serializableDirectedWeightedPseudograph
				.vertexSet();

		for (Block block : directedWeightedPseudograph.vertexSet()) {
			List<Block> blockPreds = block.getPreds();
			List<Block> blockSuccs = block.getSuccs();
			List<VertexBlock> vertexBlockPreds = new ArrayList<VertexBlock>(
					blockPreds.size());
			List<VertexBlock> vertexBlockSuccs = new ArrayList<VertexBlock>(
					blockSuccs.size());

			VertexBlock vertexBlock = getVertexBlock(block.getIndexInMethod(),
					vertexBlocks);

			for (Block blockPred : blockPreds)
				vertexBlockPreds.add(getVertexBlock(
						blockPred.getIndexInMethod(), vertexBlocks));
			for (Block blockSucc : blockSuccs)
				vertexBlockSuccs.add(getVertexBlock(
						blockSucc.getIndexInMethod(), vertexBlocks));

			vertexBlock.setPredsList(vertexBlockPreds);
			vertexBlock.setSuccsList(vertexBlockPreds);
		}

		for (CustomWeightedEdge customWeightedEdge : directedWeightedPseudograph
				.edgeSet()) {
			Block blocksource, blockTarget;
			blocksource = directedWeightedPseudograph
					.getEdgeSource(customWeightedEdge);
			blockTarget = directedWeightedPseudograph
					.getEdgeTarget(customWeightedEdge);

			VertexBlock vertexBlockSource, vertexBlockTarget;
			vertexBlockSource = getVertexBlock(blocksource.getIndexInMethod(),
					vertexBlocks);
			vertexBlockTarget = getVertexBlock(blockTarget.getIndexInMethod(),
					vertexBlocks);

			CustomWeightedEdge serializableCustomWeightedEdge = serializableDirectedWeightedPseudograph
					.addEdge(vertexBlockSource, vertexBlockTarget);
			serializableCustomWeightedEdge.setEdgeType(customWeightedEdge
					.getEdgeType());
			serializableDirectedWeightedPseudograph.setEdgeWeight(
					serializableCustomWeightedEdge, directedWeightedPseudograph
							.getEdgeWeight(customWeightedEdge));
		}

		return serializableDirectedWeightedPseudograph;
	}

	private static boolean hasExitOrReturnStatement(Block block) {
		// TODO Auto-generated method stub
		if (block.getPreds().size() == 0 || block.getSuccs().size() == 0)
			return false;

		Chain<Unit> units = block.getBody().getUnits();
		Iterator<Unit> statementIterator = units.iterator(block.getHead(),
				block.getTail());

		while (statementIterator.hasNext()) {
			Stmt statement = (Stmt) statementIterator.next();
			if (statement instanceof ReturnStmt
					|| statement instanceof ReturnVoidStmt
					|| Utilities.isExitStatement(statement))
				return true;
		}
		return false;
	}

	private static VertexBlock getVertexBlock(int indexInMethod,
			Set<VertexBlock> vertexBlocks) {
		// TODO Auto-generated method stub
		for (VertexBlock vertexBlock : vertexBlocks) {
			if (vertexBlock.getVertexBlockIndex() == indexInMethod)
				return vertexBlock;
		}

		return null;
	}

	public static ArrayList<DAGGraph> loadDAGFromFile() {
		// TODO Auto-generated method stub
		ArrayList<DAGGraph> dagGraphList = new ArrayList<DAGGraph>();
		DAGGraph dagGraph;
		try (FileInputStream fileInputStream = new FileInputStream(
				ConstantUtils.DIRECTORY_NAME + "/"
						+ ConstantUtils.DAG_GRAPH_SERIALIZABLE_FILE);
				ObjectInputStream objectInputStream = new ObjectInputStream(
						fileInputStream)) {
			while ((dagGraph = (DAGGraph) objectInputStream.readObject()) != null) {
				if (dagGraph instanceof DAGGraph)
					dagGraphList.add(dagGraph);
			}
		} catch (EOFException e) {
			// TODO: handle exception
			// e.printStackTrace();
			return dagGraphList;
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			return dagGraphList;
		}
		return dagGraphList;
	}

	public static boolean isHelperClass(String className) {
		if (className == Helper.class.getName()
				|| className == ThreadLocalMethodTupleBean.class.getName()
				|| className.contains(ConstantUtils.POP_UTIL_CLASS_NAME))
			return true;
		return false;
	}

	public static boolean checkType(Type type) {
		// TODO Auto-generated method stub
		for(String string: ConstantUtils.validTypes)
			if(type.toString().equals(string))
				return true;
		return false;
	}

	public static String createSymbolicMethodWrite(String threadId, Body body, int symbolicMethodLocalWriteCounter) {
		// TODO Auto-generated method stub
		return ConstantUtils.SYM_LOCAL_WRITE+"_"+threadId + "_" + body.getMethod().getDeclaringClass().getName()+"." + body.getMethod().getName()+"_" + symbolicMethodLocalWriteCounter;
	}

	public static String createSymbolicSharedThreadWrite(String threadId,
			String name, int symbolicSharedThreadGlobalWriteCounter) {
		// TODO Auto-generated method stub
		return ConstantUtils.SYM_SHARED_WRITE+"_"+threadId +"_" + name+"_"+symbolicSharedThreadGlobalWriteCounter;
	}

	public static String createSymbolicSharedThreadRead(String threadId,
			String name, int symbolicSharedThreadGlobalReadCounter) {
		// TODO Auto-generated method stub
		return ConstantUtils.SYM_SHARED_READ+"_"+threadId +"_" + name+"_"+symbolicSharedThreadGlobalReadCounter;
	}

}