package e0210.Utilities.bean;

import java.io.Serializable;
import java.util.List;

public class VertexBlock implements Serializable {

	private static final long serialVersionUID = -647898041875564929L;

	private int vertexBlockIndex;
	private boolean hasExitOrReturnStatement;
	private List<VertexBlock> predsList;
	private List<VertexBlock> succsList;

	public VertexBlock() {
		super();
	}

	public VertexBlock(int vertexBlockIndex, boolean hasExitOrReturnStatement) {
		super();
		this.vertexBlockIndex = vertexBlockIndex;
		this.hasExitOrReturnStatement = hasExitOrReturnStatement;
	}

	public VertexBlock(int vertexBlockIndex, List<VertexBlock> predsList,
			List<VertexBlock> succsList) {
		super();
		this.vertexBlockIndex = vertexBlockIndex;
		this.predsList = predsList;
		this.succsList = succsList;
	}

	public int getVertexBlockIndex() {
		return vertexBlockIndex;
	}

	public void setVertexBlockIndex(int vertexBlockIndex) {
		this.vertexBlockIndex = vertexBlockIndex;
	}

	public List<VertexBlock> getPredsList() {
		return predsList;
	}

	public void setPredsList(List<VertexBlock> predsList) {
		this.predsList = predsList;
	}

	public List<VertexBlock> getSuccsList() {
		return succsList;
	}

	public void setSuccsList(List<VertexBlock> succsList) {
		this.succsList = succsList;
	}

	public boolean isHasExitOrReturnStatement() {
		return hasExitOrReturnStatement;
	}

	public void setHasExitOrReturnStatement(boolean hasExitOrReturnStatement) {
		this.hasExitOrReturnStatement = hasExitOrReturnStatement;
	}

}
