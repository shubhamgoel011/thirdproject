package e0210.Utilities.bean;

public class CriticalEventTypes {
	
	public static final String BEGIN = "Begin";
	public static final String END = "End";
	public static final String READ = "Read";
	public static final String WRITE = "Write";
	public static final String LOCK = "Lock";
	public static final String UNLOCK = "Unlock";
	public static final String FORK = "Fork";
	public static final String JOIN = "Join";

}
