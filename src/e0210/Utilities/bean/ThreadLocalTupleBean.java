package e0210.Utilities.bean;

public class ThreadLocalTupleBean {

	public String threadId;
	public String criticalEventType;
	public String sharedVariableName;
	public String sharedVariableSymbolicName;
	public String childThreadID;
	public String lockObjectName;

	public int globalOrdernumber;

	public ThreadLocalTupleBean() {
		super();
	}

	public ThreadLocalTupleBean(String threadId, String criticalEventType) {
		super();
		this.threadId = threadId;
		this.criticalEventType = criticalEventType;
	}

	public ThreadLocalTupleBean(String threadId, String criticalEventType,
			String sharedVariableName, String sharedVariableSymbolicName) {
		super();
		this.threadId = threadId;
		this.criticalEventType = criticalEventType;
		this.sharedVariableName = sharedVariableName;
		this.sharedVariableSymbolicName = sharedVariableSymbolicName;
	}

	public ThreadLocalTupleBean(String threadId, String criticalEventType,
			String childThreadIDOrLocakObjectName) {
		super();
		this.threadId = threadId;
		this.criticalEventType = criticalEventType;
		if (criticalEventType.equals(CriticalEventTypes.FORK)
				|| criticalEventType.equals(CriticalEventTypes.JOIN))
			this.childThreadID = childThreadIDOrLocakObjectName;
		else if (criticalEventType.equals(CriticalEventTypes.LOCK)
				|| criticalEventType.equals(CriticalEventTypes.UNLOCK))
			this.lockObjectName = childThreadIDOrLocakObjectName;
	}

	@Override
	public String toString() {
		if (criticalEventType.equals(CriticalEventTypes.BEGIN)
				|| criticalEventType.equals(CriticalEventTypes.END))
			return threadId + ", " + criticalEventType;
		else if (criticalEventType.equals(CriticalEventTypes.READ)
				|| criticalEventType.equals(CriticalEventTypes.WRITE))
			return threadId + ", " + criticalEventType + ", "
					+ sharedVariableName + ", " + sharedVariableSymbolicName;
		else if (criticalEventType.equals(CriticalEventTypes.FORK)
				|| criticalEventType.equals(CriticalEventTypes.JOIN))
			return threadId + ", " + criticalEventType + ", " + childThreadID;
		else if (criticalEventType.equals(CriticalEventTypes.LOCK)
				|| criticalEventType.equals(CriticalEventTypes.UNLOCK))
			return threadId + ", " + criticalEventType + ", " + lockObjectName;
		return "";
	}

	public String toGlobalString() {
		// TODO Auto-generated method stub
		if (criticalEventType.equals(CriticalEventTypes.BEGIN)
				|| criticalEventType.equals(CriticalEventTypes.END))
			return threadId + ", " + criticalEventType;
		else if (criticalEventType.equals(CriticalEventTypes.READ)
				|| criticalEventType.equals(CriticalEventTypes.WRITE))
			return threadId + ", " + criticalEventType + ", "
					+ sharedVariableName;
		else if (criticalEventType.equals(CriticalEventTypes.FORK)
				|| criticalEventType.equals(CriticalEventTypes.JOIN))
			return threadId + ", " + criticalEventType + ", " + childThreadID;
		else if (criticalEventType.equals(CriticalEventTypes.LOCK)
				|| criticalEventType.equals(CriticalEventTypes.UNLOCK))
			return threadId + ", " + criticalEventType + ", " + lockObjectName;
		return "";
	}

	public String toGlobalStringWithOrderNumber() {
		// TODO Auto-generated method stub
		if (criticalEventType.equals(CriticalEventTypes.BEGIN)
				|| criticalEventType.equals(CriticalEventTypes.END))
			return String.valueOf(globalOrdernumber) + ", " + threadId + ", "
					+ criticalEventType;
		else if (criticalEventType.equals(CriticalEventTypes.READ)
				|| criticalEventType.equals(CriticalEventTypes.WRITE))
			return String.valueOf(globalOrdernumber) + ", " + threadId + ", " + criticalEventType + ", "
					+ sharedVariableName;
		else if (criticalEventType.equals(CriticalEventTypes.FORK)
				|| criticalEventType.equals(CriticalEventTypes.JOIN))
			return String.valueOf(globalOrdernumber) + ", " + threadId + ", " + criticalEventType + ", " + childThreadID;
		else if (criticalEventType.equals(CriticalEventTypes.LOCK)
				|| criticalEventType.equals(CriticalEventTypes.UNLOCK))
			return String.valueOf(globalOrdernumber) + ", " + threadId + ", " + criticalEventType + ", " + lockObjectName;
		return "";
	}

}
