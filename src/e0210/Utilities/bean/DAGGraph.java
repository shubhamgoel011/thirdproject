package e0210.Utilities.bean;

import java.io.Serializable;

import org.jgrapht.graph.DirectedWeightedPseudograph;

import e0210.graph.CustomWeightedEdge;

public class DAGGraph implements Serializable {

	private static final long serialVersionUID = -5952795076281305807L;

	private long startBallLarusIdentifier;
	private long endBallLarusIdentifier;
	private DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> serializableDirectedWeightedPseudograph;

	public DAGGraph(
			long startBallLarusIdentifier,
			long endBallLarusIdentifier,
			DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> serializableDirectedWeightedPseudograph) {
		super();
		this.startBallLarusIdentifier = startBallLarusIdentifier;
		this.endBallLarusIdentifier = endBallLarusIdentifier;
		this.serializableDirectedWeightedPseudograph = serializableDirectedWeightedPseudograph;
	}

	public long getStartBallLarusIdentifier() {
		return startBallLarusIdentifier;
	}

	public void setStartBallLarusIdentifier(long startBallLarusIdentifier) {
		this.startBallLarusIdentifier = startBallLarusIdentifier;
	}

	public long getEndBallLarusIdentifier() {
		return endBallLarusIdentifier;
	}

	public void setEndBallLarusIdentifier(long endBallLarusIdentifier) {
		this.endBallLarusIdentifier = endBallLarusIdentifier;
	}

	public DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> getSerializableDirectedWeightedPseudograph() {
		return serializableDirectedWeightedPseudograph;
	}

	public void setSerializableDirectedWeightedPseudograph(
			DirectedWeightedPseudograph<VertexBlock, CustomWeightedEdge> serializableDirectedWeightedPseudograph) {
		this.serializableDirectedWeightedPseudograph = serializableDirectedWeightedPseudograph;
	}

}
