package e0210.Utilities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import soot.Body;
import soot.Local;
import soot.PatchingChain;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.AssignStmt;
import soot.jimple.GotoStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.LongConstant;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.VirtualInvokeExpr;
import e0210.soot.Helper;

public class SootUtilities {

	static SootClass helperSootClass;
	static SootMethod printMethodBallLarusIdentifier,
			initializeMainMethodInvocation, threadStartMethodInvoked,
			methodInvokedInThread, getNumOfTimesThisMethodCalledByThisThread,
			getNumOfMethodsInvokedByThisThread, printThreadLocalMethodTuple;

	static {
		helperSootClass = Scene.v().getSootClass(Helper.class.getName());
		printMethodBallLarusIdentifier = helperSootClass
				.getMethod("void printMethodBallLarusIdentifier(long)");
		initializeMainMethodInvocation = helperSootClass
				.getMethod("void initializeMainMethodInvocation(java.lang.String)");
		threadStartMethodInvoked = helperSootClass
				.getMethod("void threadStartMethodInvoked(long)");
		methodInvokedInThread = helperSootClass
				.getMethod("void methodInvokedInThread(java.lang.String)");
		getNumOfTimesThisMethodCalledByThisThread = helperSootClass
				.getMethod("long getNumOfTimesThisMethodCalledByThisThread(java.lang.String)");
		getNumOfMethodsInvokedByThisThread = helperSootClass
				.getMethod("long getNumOfMethodsInvokedByThisThread(java.lang.String)");
		printThreadLocalMethodTuple = helperSootClass
				.getMethod("void printThreadLocalMethodTuple(java.lang.String,long,long,long)");
	}

	public static Unit getMethodBallLarusPrintStatement(
			Local methodBallLarusIdentifier) {
		// TODO Auto-generated method stub
		InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(
				printMethodBallLarusIdentifier.makeRef(),
				methodBallLarusIdentifier);
		Stmt invokeStatement = Jimple.v().newInvokeStmt(invokeExpr);
		return invokeStatement;
	}

	public static void printMethodBallLarusPrintStatement(Body body,
			Stmt statement, Local methodBallLarusIdentifier) {
		// TODO Auto-generated method stub
		body.getUnits().insertBefore(
				getMethodBallLarusPrintStatement(methodBallLarusIdentifier),
				statement);
	}

	public static void initializeMethodBallLarusIdentifier(Body body,
			Local methodBallLarusIdentifier, Long value) {
		// TODO Auto-generated method stub
		Stmt firstNonIdentityStatement = null, secondNonIdentityStatement = null, previosStatement = null;
		PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> statementIterator = units.snapshotIterator();
		while (statementIterator.hasNext()) {
			Stmt statement = (Stmt) statementIterator.next();
			if (!(statement instanceof IdentityStmt)) {
				firstNonIdentityStatement = statement;
				break;
			}
			previosStatement = statement;
		}
		if(previosStatement == null)
			body.getUnits().insertBefore(
				(Jimple.v().newAssignStmt(methodBallLarusIdentifier,
						LongConstant.v(value))), firstNonIdentityStatement);
		else
			body.getUnits().insertAfter(
					(Jimple.v().newAssignStmt(methodBallLarusIdentifier,
							LongConstant.v(value))), previosStatement);
		
		
		
		units = body.getUnits();
		statementIterator = units.snapshotIterator();
		while (statementIterator.hasNext()) {
			Stmt statement = (Stmt) statementIterator.next();
			if (!(statement instanceof IdentityStmt)) {
				firstNonIdentityStatement = statement;
				break;
			}
		}
		
		secondNonIdentityStatement =  (Stmt) statementIterator.next();
		GotoStmt gotoStmt = Jimple.v().newGotoStmt(secondNonIdentityStatement);
		body.getUnits().insertAfter(gotoStmt, firstNonIdentityStatement);
		return;
	}

	public static void initializeMethodInvocationNumberByThisThread(Body body,
			Local methodInvocationNumberByThisThread,
			Local numOfTimesThisMethodCalledByThisThread) {
		// TODO Auto-generated method stub
		PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> statementIterator = units.snapshotIterator();
		while (statementIterator.hasNext()) {
			Stmt statement = (Stmt) statementIterator.next();
			if (!(statement instanceof IdentityStmt)) {
				body.getUnits().insertBefore(
						getUnitsToInitializeMethodInvocationNumberByThisThread(
								body, methodInvocationNumberByThisThread,
								numOfTimesThisMethodCalledByThisThread),
						statement);
				return;
			}
		}
		return;
	}

	private static List<Unit> getUnitsToInitializeMethodInvocationNumberByThisThread(
			Body body, Local methodInvocationNumberByThisThread,
			Local numOfTimesThisMethodCalledByThisThread) {
		// TODO Auto-generated method stub
		List<Unit> units = new ArrayList<Unit>();
		if (Utilities.isMainMethod(body))
			units.add(getInitializeMainMethodInvocationStatement(body));
		units.add(getMethodInvocationNumberByThisThreadStatement(body,
				methodInvocationNumberByThisThread));
		units.add(getNumOfTimesThisMethodCalledByThisThreadStatement(body,
				numOfTimesThisMethodCalledByThisThread));
		units.add(getMethodInvokedInThreadStatement(body));
		return units;
	}

	private static Unit getInitializeMainMethodInvocationStatement(Body body) {
		// TODO Auto-generated method stub
		InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(
				initializeMainMethodInvocation.makeRef(),
				StringConstant.v(body.getMethod().getSignature()));
		Stmt invokeStatement = Jimple.v().newInvokeStmt(invokeExpr);
		return invokeStatement;
	}

	private static Unit getNumOfTimesThisMethodCalledByThisThreadStatement(
			Body body, Local numOfTimesThisMethodCalledByThisThread) {
		// TODO Auto-generated method stub
		InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(
				getNumOfTimesThisMethodCalledByThisThread.makeRef(),
				StringConstant.v(body.getMethod().getSignature()));
		// Stmt invokeStatement = Jimple.v().newInvokeStmt(invokeExpr);

		AssignStmt assignStmt = Jimple.v().newAssignStmt(
				numOfTimesThisMethodCalledByThisThread, invokeExpr);
		return assignStmt;
	}

	private static Unit getMethodInvocationNumberByThisThreadStatement(
			Body body, Local methodInvocationNumberByThisThread) {
		// TODO Auto-generated method stub
		InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(
				getNumOfMethodsInvokedByThisThread.makeRef(),
				StringConstant.v(body.getMethod().getSignature()));
		// Stmt invokeStatement = Jimple.v().newInvokeStmt(invokeExpr);

		AssignStmt assignStmt = Jimple.v().newAssignStmt(
				methodInvocationNumberByThisThread, invokeExpr);
		return assignStmt;
	}

	private static Unit getMethodInvokedInThreadStatement(Body body) {
		// TODO Auto-generated method stub
		InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(
				methodInvokedInThread.makeRef(),
				StringConstant.v(body.getMethod().getSignature()));
		Stmt invokeStatement = Jimple.v().newInvokeStmt(invokeExpr);
		return invokeStatement;
	}

	public static void newThreadInvocationInThisMethod(Body body,
			Local childThreadId) {
		// TODO Auto-generated method stub
		PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> statementIterator = units.snapshotIterator();
		while (statementIterator.hasNext()) {
			Stmt statement = (Stmt) statementIterator.next();
			if (statement instanceof InvokeStmt) {
				InvokeExpr invokeExpr = statement.getInvokeExpr();
				if (invokeExpr.getMethod().getSignature()
						.equals(ConstantUtils.THREAD_START_METHOD_NAME)) {
					Local threadObject = (Local) ((VirtualInvokeExpr) invokeExpr)
							.getBase();

					AssignStmt assignStmt = Jimple
							.v()
							.newAssignStmt(
									childThreadId,
									Jimple.v()
											.newVirtualInvokeExpr(
													threadObject,
													Scene.v()
															.getMethod(
																	ConstantUtils.THREAD_GETID_METHOD_NAME)
															.makeRef()));

					InvokeExpr threadStartMethodInvokeExpr = Jimple.v()
							.newStaticInvokeExpr(
									threadStartMethodInvoked.makeRef(),
									childThreadId);
					Stmt invokeStatement = Jimple.v().newInvokeStmt(
							threadStartMethodInvokeExpr);
					body.getUnits().insertBefore(assignStmt, statement);
					body.getUnits().insertBefore(invokeStatement, statement);
				}
			}
		}
		return;
	}

	public static Unit getThreadTupleOfThisMethod(Body body,
			Local methodInvocationNumberByThisThread,
			Local numOfTimesThisMethodCalledByThisThread,
			Local methodBallLarusIdentifier) {
		InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(
				printThreadLocalMethodTuple.makeRef(),
				StringConstant.v(body.getMethod().getSignature()),
				numOfTimesThisMethodCalledByThisThread,
				methodInvocationNumberByThisThread, methodBallLarusIdentifier);
		Stmt invokeStatement = Jimple.v().newInvokeStmt(invokeExpr);
		return invokeStatement;
	}

	public static void printThreadTupleOfThisMethod(Body body,
			Local methodInvocationNumberByThisThread,
			Local numOfTimesThisMethodCalledByThisThread,
			Local methodBallLarusIdentifier) {
		// TODO Auto-generated method stub
		PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> statementIterator = units.snapshotIterator();

		while (statementIterator.hasNext()) {
			Stmt statement = (Stmt) statementIterator.next();

			if (statement instanceof ReturnStmt
					|| statement instanceof ReturnVoidStmt
					|| Utilities.isExitStatement(statement)) {
				InvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(
						printThreadLocalMethodTuple.makeRef(),
						StringConstant.v(body.getMethod().getSignature()),
						numOfTimesThisMethodCalledByThisThread,
						methodInvocationNumberByThisThread,
						methodBallLarusIdentifier);
				Stmt invokeStatement = Jimple.v().newInvokeStmt(invokeExpr);
				body.getUnits().insertBefore(invokeStatement, statement);
			}

		}
	}
}
